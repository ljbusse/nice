function sse=fitGaussian(params,Input,Actual_Output)
height=params(1);
center=params(2);
width=params(3);
Fitted_Curve=height*exp(-(Input-center).^2/(2*width.^2));
Error_Vector=Fitted_Curve - Actual_Output;
% When curvefitting, a typical quantity to
% minimize is the sum of squares error
sse=sum(Error_Vector.^2);
% You could also write sse as
% sse=Error_Vector(:)'*Error_Vector(:);