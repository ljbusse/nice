function varargout = NICE_GUI_B2b(varargin)
% NICE is a bacterial colony counting program as a GUI
%
%       NICE is designed to count colonies from multiple regions of
%       interest.  E.g., 24 'streaks' of stained colonies on an agar plate.
%
%       NICE is designed to count dark colonies from multiple regions of 
%       interests (ROIs).
%       For example, 24 "streaks" of stained colonies on an agar plate.  
%       Images can be acquired by a digital camera or a desktop scanner.
% 
%       The primary application of NICE is for opsonophagocytic killing 
%       assays.
%
%     NICE was developed by:
%     Matthew Clarke* (matthew.clarke@nist.gov) and 
%     Jeeseong Hwang (jeeseong.hwang@nist.gov)
%     Optical Technology Division
%     National Institute of Standards and Technology
%     Gaithersburg, MD 20899
% 
%     In collaboration with:
%     Robert Burton and Moon Nahm, University of Alabama � Birmingham
% 
%     Citation:
%     Matthew L. Clarke, Robert L. Burton, A. Nayo Hill, Maritoni Litorja, 
%     Moon H. Nahm,Jeeseong Hwang, 
%     "Low-Cost, High-Throughput, Automated Counting of Bacterial Colonies" 
%     Cytometry A, 77A, 790-797 (2010)
%
%     *Program code was developed by and correspondence should be addressed
%     to Matthew Clarke.  Correspondence may be sent to nice@nist.gov.
% 
%     Support for this project was provided in part by PATH.  The views 
%     expressed by the authors do not necessarily reflect the views of 
%     PATH.
%
%     Created and modified, 2008-2010
%
%  Legal Disclaimer:
%  NIST�s Integrated Colony Enumerator (NICE) software was developed at the
%  National Institute of Standards and Technology by employees of the
%  Federal Government in the course of their official duties. Pursuant to
%  Title 17 Section 105 of the United States Code this software is not
%  subject to copyright protection and is in the public domain. Support for
%  this project was provided in part by PATH.  The views expressed by the
%  developers of the software do not necessarily reflect the views of PATH.
%  NICE is an experimental system. Neither NIST nor PATH assumes any
%  responsibility whatsoever for its use by other parties, and makes no
%  guarantees, expressed or implied, about its quality, reliability, or any
%  other characteristic. We would appreciate acknowledgment if the software
%  is used. This software can be redistributed and/or modified freely
%  provided that any derivative works bear some notice that they are
%  derived from it, and any modified versions bear some notice that they
%  have been modified. 
%
% NICE_GUI_B2b M-file for NICE_GUI_B2b.fig
%      NICE_GUI_B2b, by itself, creates a new NICE_GUI_B2b or raises the
%      existing
%      singleton*.
%
%      H = NICE_GUI_B2b returns the handle to a new NICE_GUI_B2b or the
%      handle to
%      the existing singleton*.
%
%      NICE_GUI_B2b('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NICE_GUI_B2b.M with the given input arguments.
%
%      NICE_GUI_B2b('Property','Value',...) creates a new NICE_GUI_B2b or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before NICE_GUI_B2b_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to NICE_GUI_B2b_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help NICE_GUI_B2b

% Last Modified by GUIDE v2.5 16-Feb-2017 13:47:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @NICE_GUI_B2b_OpeningFcn, ...
                   'gui_OutputFcn',  @NICE_GUI_B2b_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before NICE_GUI_B2b is made visible.
function NICE_GUI_B2b_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to NICE_GUI_B2b (see VARARGIN)
handles.expdetails = colony_welcome; %opens the login window
[handles]=getpositions(hObject,handles); %get locations of panels, etc.
%figure resizing is initially not active (to prevent some errors), now
%activate to allow resizing
set(handles.figure1, 'Resize', 'on', 'ResizeFcn', @figure1_ResizeFcn);
% Load file and display
% handles.FileName=('democount.tif');  %load demo image, scanner
% handles.FileNamenoext=('democount');
handles.FileName=('demo-count-cam.JPG');  %load demo image, camera
handles.FileNamenoext=('demo-count-cam'); %filename without extension
handles.PathName=pwd;               %show current directory
handles.Path1st=pwd;               %get first directory
% handles.I=uint8(imread(handles.FileName));  %read file, save to handles, scanner
handles.I=uint8(rgb2gray(imread(handles.FileName)));  %read file, save to handles, camera
[handles]=newaxis(hObject,handles);  %resize image axis to best fit data
Ilogo=imread('NICElogo1.jpg');  %NICE logo in upper left corner
axes(handles.axesLogo); %get logo axes
imshow(Ilogo); %display NICE logo
axis off
zoomfunc = zoom;
setAllowAxesZoom(zoomfunc,handles.axesLogo,false); %prevent zooming on logo
[handles]=initialize_gui(hObject, handles); %load default settings
[handles]=plothist(hObject, handles, 'on'); %plot histogram with or without lines on/off line plots

guidata(hObject,handles)

% Choose default command line output for colonyROI_count_deploya8u
handles.output = hObject;
handles.filecount=1; %index for number of files opened after program launch
handles.countcount=0; %index for whether file was counted
filecount_string = sprintf('%03.0f',handles.filecount);  %convert num to string
handles.numbered_filename=([filecount_string, '-',handles.FileNamenoext]);
handles.countedfiles=cell(1);

guidata(hObject, handles);  % Update handles structure

% UIWAIT makes NICE_GUI_B2b wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = NICE_GUI_B2b_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- initialize_gui executes in the Opening function.
% --- It sets many default values
function [handles]=initialize_gui(hObject, handles)
handles.subrow = 4; %number of ROI rows
handles.subcol = 12; %number of ROI columns
handles.histlog=0;  %histogram should have normal y-axis (not log)
handles.parentroi=0; %parent roi not active
handles.parentrect=0; %parent roi does not exist
handles.parentresizing=0; %parent ROI is not currently being resized;
handles.sizeon=0; %find colony sizes
handles.invert=0; %no contrast inversion
% the following are the resolution values used in calculations
%values are [gausian blur matrix size, gaussian width, minima scalar, connectivity]
handles.resop=[3 5 5 4; 3 5 10 4; 5 5 14 4]; %low, mid, high (~300, 600, 1200 dpi)
set(handles.textHelp, 'String', 'Demo image loaded: Camera Image'); %Display help text
set(handles.uipushtoolFound, 'Enable', 'off'); %disable processed image button
set(handles.uipushtoolHigh, 'Enable', 'off');  %disable Highlight image button
set(handles.popupmenuBkgd, 'Value', 1);  %set bkgd to -3 sigma
handles.bkgdchoice=1;               %set bkgd choice to default
set(handles.popupmenuResolution, 'Value', 1);  %set resolution to low
handles.reschoice=1;               %set res choice to default
handles.ROI_shape='rectangle'; %ROIs are rectangles by default
handles.ROI_parentshape='rectangle'; %ROIs are rectangles by default
handles.batch=0;  %0 for single file, 1 for batch
set(handles.textBatch, 'String', 'Single File'); %Demo image is a single file
[handles]=pushbuttonTabImages_Callback(hObject, [], handles); %display Images tab
[handles]=toolsoff(hObject, handles); %deselect any toolbar buttons
%set(handles.figure1, 'WindowButtonDownFcn',@roibuttondown); %move ROI function
set(handles.figure1, 'WindowButtonDownFcn',@LJBtesting); %move ROI function
handles.activeROI=[1 1];  %set the first ROI as the active ROI
[handles]=fastroi(hObject, handles);  %calculate ROI for the image
[handles]=plotsubIrect(hObject, handles);  %plot ROIs
guidata(hObject,handles);  % Update handles structure


% --- getpositions executes in the Opening function.
% --- It records the positions of all panels, etc.
function [handles]=getpositions(hObject,handles)
%NICE opens with all panels, text as 'normalized', to optimize display
%now set to 'pixels' for all resizing calculations
set(handles.uipanelCount,'Units','pixels');
set(handles.uipanelStored,'Units','pixels');
set(handles.listboxData,'Units','pixels');
set(handles.uipanelParam,'Units','pixels');
set(handles.uipanelROI,'Units','pixels');
set(handles.uipanelTitle,'Units','pixels');
set(handles.figure1,'Units','pixels');
set(handles.axes1,'Units','pixels');
set(handles.axesHist,'Units','pixels');
set(handles.axesLogo,'Units','pixels');
set(handles.textHelp,'Units','pixels');
set(handles.pushbuttonTabResults,'Units','pixels');
set(handles.pushbuttonTabImages,'Units','pixels');
set([handles.pushbuttonTabImages, handles.pushbuttonTabResults, handles.pushbuttonExport, handles.pushbuttonDelete, handles.pushbuttonCleardata], 'Units', 'pixels');
set(handles.checkboxLog,'Units','pixels');
handles.Countpanel=get(handles.uipanelCount, 'Position');
handles.Storedpanel=get(handles.uipanelStored, 'Position');
handles.listboxpos=get(handles.listboxData, 'Position');
handles.Parampanel=get(handles.uipanelParam, 'Position');
handles.ROIpanel=get(handles.uipanelROI, 'Position');
handles.Titlepanel=get(handles.uipanelTitle, 'Position');
handles.figpos=get(handles.figure1, 'Position'); %keep original Figure1 Position
handles.axisorig=get(handles.axes1, 'Position'); %keep original axis1 Position
handles.axispos=get(handles.axes1, 'Position'); %keep current axis1 Position
handles.histpos=get(handles.axesHist, 'Position');
handles.logopos=get(handles.axesLogo, 'Position');
handles.helppos=get(handles.textHelp, 'Position');
handles.tabRpos=get(handles.pushbuttonTabResults, 'Position');
handles.tabIpos=get(handles.pushbuttonTabImages, 'Position');

% --- Executes on button press in pushbuttonStart.
function pushbuttonStart_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles]=navigate_button(hObject, handles, -2); %Go to first image
guidata(hObject,handles)

% --- Executes on button press in pushbuttonBack.
function pushbuttonBack_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles]=navigate_button(hObject, handles, -1); %go back one image
guidata(hObject,handles)

% --- Executes on button press in pushbuttonForward.
function pushbuttonForward_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonForward (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles]=navigate_button(hObject, handles, 1); %go forward one image
guidata(hObject,handles)

% --- Executes on button press in pushbuttonEnd.
function pushbuttonEnd_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles]=navigate_button(hObject, handles, 2); %go to last image
guidata(hObject,handles)


%----navigate_button------
%moves through images.  called by several pushbuttons
function [handles]=navigate_button(hObject, handles, direction)
set(handles.figure1, 'Pointer', 'watch');
pause(0.01);
switch handles.tab
    case 'Images' % Listbox is displaying Images list
        if(handles.batch==0);  %Navigation only useful for batch (0 for single file, 1 for batch)
        elseif(min(direction~=[-2 -1 1 2])) %is direction valid?
            errordlg('Error with navigation direction','Navigation Error', 'modal')
        %if valid, check if already at the "new" image
        elseif((max(direction==[-2 -1]) && handles.filenum==1) || (max(direction==[1 2]) && handles.filenum==length(handles.FileNames)));
        else
            %save the current ROIs before moving to next image
            if(handles.parentroi==1);
                handles.roistruc{handles.filenum,2}=handles.parentrect; %save parent ROI
                lastparent=handles.parentrect; %keep the last parent in case a parent is missing
            else
            end
            handles.roistruc{handles.filenum,1}=handles.subIrect; %save ROI structure
            %Go to the appropriate image
            if(max(direction==[-1 1]));
                handles.filenum=handles.filenum+direction;
            elseif(direction==-2)
                handles.filenum=1;
            elseif(direction==2)
                handles.filenum=length(handles.FileNames);
            end
            handles.FileName=handles.FileNames{handles.filenum}; %load filename
            Nfile=handles.filenum; %change filenumber
            [handles]=fileopen(hObject, handles, 'no'); %open file (without calculating ROIs)
            [handles]=plothist(hObject, handles, 'on'); %plot histogram with or without lines on/off line plots
            batch_string=['Batch File ', num2str(handles.filenum),' of ', num2str(length(handles.FileNames))];
            set(handles.textBatch, 'string', batch_string); %display File X of Y
            filecount_string = sprintf('%03.0f',handles.filecount);  %convert num to string
            handles.numbered_filename=([filecount_string, '-',handles.FileNamenoext]);
            handles.subIrect=handles.roistruc{Nfile,1};  %load ROIs
            handles.parentrect=handles.roistruc{Nfile,2}; %load parent ROI
            if(isempty(handles.parentrect)==1  && handles.parentroi==1) %check if parent is needed
                if(exist('lastparent', 'var')) 
                    %if a parent roi existed in the last image use that one
                    handles.parentrect=lastparent;
                    [handles]=fastroi(hObject, handles);
                else
                    %otherwise use the image size as the parent
                    handles.parentrect=[1 1 size(handles.I,2)-1 size(handles.I,1)-1];
                    [handles]=fastroi(hObject, handles);
                end
            end
            %if no ROIS exist, then calculate ROIs
            if(isempty(handles.subIrect)==1)
                [handles]=fastroi(hObject, handles);
            end
            %If stored ROI grid does not match current ROI grid then change
            if(handles.subrow~=size(handles.subIrect,1))
                set(handles.textHelp, 'String', 'Stored and set ROI #s do not match. Recalculated ROIs.')
                [handles]=fastroi(hObject, handles);
            elseif(handles.subcol~=size(handles.subIrect,2))
                set(handles.textHelp, 'String', 'Stored and set ROI #s do not match. Recalculated ROIs.')
                [handles]=fastroi(hObject, handles);
            end
            [handles]=plotsubIrect(hObject, handles); %plot the new image
            set(handles.listboxData,'Value',handles.filenum); %change selected listbox value
        end
    case 'Results' % Listbox is displaying Results list
        index_selected=get(handles.listboxData, 'Value'); %find currently selected listbox value
        if(length(index_selected)>1) %if more than one entry was selected, us the first one selected
            index_selected=index_selected(1);
        end
        listbox_values=get(handles.listboxData, 'String');
        if(strcmp(get(handles.listboxData, 'String'), 'cleared')) %check if results exist
        elseif(min(direction~=[-2 -1 1 2])) %check if direction is valid
            errordlg('Error with navigation direction','Navigation Error', 'modal') 
        %if valid, check if already at the "new" image
        elseif((max(direction==[-2 -1]) && index_selected==1) || (max(direction==[1 2]) && index_selected==length(listbox_values)));
        else
            %Go to the appropriate image
            if(max(direction==[-1 1]));
                index_selected=index_selected+direction;
            elseif(direction==-2)
                index_selected=1;
            elseif(direction==2)
                index_selected=length(listbox_values);
            end
            handles.FileName=char(handles.otherout{handles.goodlist(index_selected)}{1}); %load filename
            PathName=char(handles.otherout{handles.goodlist(index_selected)}{8}); %load path
            cd(PathName); %change directory to path
            [handles]=fileopen(hObject, handles); %open file (does not calc new ROIs)
            [handles]=plothist(hObject, handles, 'on'); %plot histogram with or without lines on/off line plots
            if (handles.filecount==handles.countcount);
                handles.filecount=handles.filecount+1; %index for number of files opened & counted after program launch
            else
                handles.filecount=handles.filecount; %index for number of files opened after program launch
            end
            filecount_string = sprintf('%03.0f',handles.filecount);  %need string for text overlay
            handles.numbered_filename=([filecount_string, '-',handles.FileNamenoext]);
            handles.subIrect=handles.otherout{handles.goodlist(index_selected)}{3}; %load ROIs
            handles.parentrect=handles.otherout{handles.goodlist(index_selected)}{5}; %load parent ROI
            handles.counts_string=handles.otherout{handles.goodlist(index_selected)}{4}; %load counts
            handles.subrow=size(handles.subIrect,1); %load ROI rows
            handles.subcol=size(handles.subIrect,2); %load ROI columns
            handles.centroids=handles.otherout{handles.goodlist(index_selected)}{6}; %load saved data
            [handles]=plotsubIrect(hObject, handles); %plot result with ROIs
            handles.batch=0;  %0 for single file, 1 for batch
            set(handles.listboxData,'Value',index_selected);
            set(handles.textBatch, 'String', 'Stored File');
            set(handles.uipushtoolHigh, 'Enable', 'on');
            set(handles.textHelp, 'String', 'Displaying stored file.');
        end
    otherwise
       errordlg('Error with handles.tab','Navigation Error', 'modal') 
end
set(handles.figure1, 'Pointer', 'arrow');
guidata(hObject,handles)

% --- Executes on button press in pushbuttonApplyall.
% --- Copies the ROIs for this image to all loaded files
function pushbuttonApplyall_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonApplyall (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(handles.batch==0);  %0 for single file, 1 for batch
else
    for files=1:length(handles.FileNames)
        if(handles.parentroi==1);
            handles.roistruc{files,2}=handles.parentrect;
            handles.roistruc{files,1}=handles.subIrect;
        else
            handles.roistruc{files,1}=handles.subIrect;
        end
    end
    [handles]=plotsubIrect(hObject, handles);
end
guidata(hObject,handles);

% --- Executes on button press in pushbuttonApplyrem.
% --- Copies the ROIs for this image to  files after this one
function pushbuttonApplyrem_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonApplyrem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(handles.batch==0);  %0 for single file, 1 for batch
else
    for files=handles.filenum:length(handles.FileNames)
        if(handles.parentroi==1);
            handles.roistruc{files,2}=handles.parentrect;
        else
        end
        handles.roistruc{files,1}=handles.subIrect;
    end
end
guidata(hObject,handles);


% --- Executes on selection change in listboxData.
function listboxData_Callback(hObject, eventdata, handles)
% hObject    handle to listboxData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = get(hObject,'String') returns listboxData contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listboxData
switch handles.tab
    case 'Images' % Listbox is displaying Images list
        get(handles.figure1,'SelectionType');
        set(handles.uipushtoolFound, 'Enable', 'off');
        if strcmp(get(handles.figure1,'SelectionType'),'open')
            index_selected = get(handles.listboxData,'Value');
            if(max(size(index_selected))>1)
                index_selected=index_selected(1);
            end
            if(handles.batch==0);  %0 for single file, 1 for batch
            else
                %save the current ROIs before moving to next image
                if(handles.parentroi==1);
                    handles.roistruc{handles.filenum,2}=handles.parentrect;
                    lastparent=handles.parentrect; %keep the last parent in case a parent is missing
                else
                end
                handles.roistruc{handles.filenum,1}=handles.subIrect;
                %Go to the next image
                handles.filenum=index_selected;
                handles.FileName=handles.FileNames{handles.filenum}; %load filename
                Nfile=handles.filenum; %change filenumber
                [handles]=fileopen(hObject, handles, 'no'); %open file (without calculating ROIs)
                [handles]=plothist(hObject, handles, 'on'); %plot histogram with or without lines on/off line plots
                batch_string=['Batch File ', num2str(handles.filenum),' of ', num2str(length(handles.FileNames))];
                set(handles.textBatch, 'string', batch_string); %display File X of Y
                filecount_string = sprintf('%03.0f',handles.filecount);  %convert num to string
                handles.numbered_filename=([filecount_string, '-',handles.FileNamenoext]);
                handles.subIrect=handles.roistruc{Nfile,1};  %load ROIs
                handles.parentrect=handles.roistruc{Nfile,2}; %load parent ROI
                if(isempty(handles.parentrect)==1  && handles.parentroi==1) %check if parent is needed
                    if(exist('lastparent', 'var')) 
                        %if a parent roi existed in the last image use that one
                        handles.parentrect=lastparent;
                        [handles]=fastroi(hObject, handles);
                    else
                        %otherwise use the image size as the parent
                        handles.parentrect=[1 1 size(handles.I,2)-1 size(handles.I,1)-1];
                        [handles]=fastroi(hObject, handles);
                    end
                end
                %if no ROIS exist, then calculate ROIs
                if(isempty(handles.subIrect)==1)
                    [handles]=fastroi(hObject, handles);
                end
                %If stored ROI grid does not match current ROI grid then change
                if(handles.subrow~=size(handles.subIrect,1))
                    set(handles.textHelp, 'String', 'Stored and set ROI #s do not match. Recalculated ROIs.')
                    [handles]=fastroi(hObject, handles);
                elseif(handles.subcol~=size(handles.subIrect,2))
                    set(handles.textHelp, 'String', 'Stored and set ROI #s do not match. Recalculated ROIs.')
                    [handles]=fastroi(hObject, handles);
                end
                [handles]=plotsubIrect(hObject, handles); %plot the new image
            end
        end
    case 'Results' % Listbox is displaying Results list
        if(strcmp(get(handles.listboxData, 'String'), 'cleared')==0);
            get(handles.figure1,'SelectionType');
            set(handles.uipushtoolFound, 'Enable', 'off');
            if strcmp(get(handles.figure1,'SelectionType'),'open')
            index_selected = get(handles.listboxData,'Value');
            if(max(size(index_selected))>1)
                index_selected=index_selected(1);
            end
            handles.FileName=char(handles.otherout{handles.goodlist(index_selected)}{1}); %load filename
            handles.filenum=index_selected;
            PathName=char(handles.otherout{handles.goodlist(index_selected)}{8}); %load path
            handles.ROI_shape=handles.otherout{handles.goodlist(index_selected)}{7}(1);
            handles.ROI_parentshape=handles.otherout{handles.goodlist(index_selected)}{7}(2);
            cd(PathName); %change directory to path
            [handles]=fileopen(hObject, handles); %open file (does not calc new ROIs)
            [handles]=plothist(hObject, handles, 'on'); %plot histogram with or without lines on/off line plots
            if (handles.filecount==handles.countcount);
                handles.filecount=handles.filecount+1; %index for number of files opened & counted after program launch
            else
                handles.filecount=handles.filecount; %index for number of files opened after program launch
            end
            filecount_string = sprintf('%03.0f',handles.filecount);  %need string for text overlay
            handles.numbered_filename=([filecount_string, '-',handles.FileNamenoext]);
            handles.subIrect=handles.otherout{handles.goodlist(index_selected)}{3}; %load ROIs
            handles.parentrect=handles.otherout{handles.goodlist(index_selected)}{5}; %load parent ROI
            handles.counts_string=handles.otherout{handles.goodlist(index_selected)}{4}; %load counts
            handles.subrow=size(handles.subIrect,1); %load ROI rows
            handles.subcol=size(handles.subIrect,2); %load ROI columns
%             handles = rmfield(handles, 'data'); %clear previous data
            handles.centroids=handles.otherout{handles.goodlist(index_selected)}{6}; %load saved data
            if (handles.filecount==handles.countcount);
                handles.filecount=handles.filecount+1; %index for number of files opened & counted after program launch
            else
                handles.filecount=handles.filecount; %index for number of files opened after program launch
            end
            [handles]=plotsubIrect(hObject, handles);
            handles.batch=0;  %0 for single file, 1 for batch
            set(handles.textBatch, 'String', 'Stored File');
            set(handles.uipushtoolHigh, 'Enable', 'on');
            set(handles.textHelp, 'String', 'Displaying stored file.');
            end
        end
    otherwise
       errordlg('Error with handles.tab','Listbox Error', 'modal') 
end
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function listboxData_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listboxData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbuttonCleardata.
function pushbuttonCleardata_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonCleardata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(handles.countcount~=0)
    handles.question=('Clear all data?');
    %send question to confirm_gui, which will display a yes/no window
    handles.clear = confirm_gui([],[],[],handles.question); %3 blank, then question
    if(handles.clear==1); %user selected yes
        [handles]=cleardata(hObject, handles);
        set(handles.textHelp, 'String', 'All stored data has been cleared.');
        set(handles.uipushtoolHigh, 'Enable', 'off');
        set(handles.uipushtoolFound, 'Enable', 'off');
    else
    end
else
end
guidata(hObject,handles)

% --------------------------------------------------------------------
function [handles]=cleardata(hObject, handles)
%reset file counts
handles.filecount=1; %index for number of files opened after program launch
handles.countcount=0; %index for whether file was counted
%remove memory intensive structures and data lists
handles = rmfield(handles, {'outputstruc'; 'countedfiles'; 'otherout'; 'fulllist'; 'goodlist'});
%create new cell for later counts
handles.countedfiles=cell(1);
set(handles.listboxData, 'Value', 1);
set(handles.listboxData, 'String', 'cleared');
guidata(hObject,handles)


% --- Executes on button press in pushbuttonDelete.
function pushbuttonDelete_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(handles.countcount~=0) %check if any results have been generated
    index_selected = get(handles.listboxData,'Value');
    if(max(size(index_selected))>1) %more than one result is selected
        handles.question=['Clear multiple results?'];
        %send question to confirm_gui, which will display a yes/no window
        delcon = confirm_gui([],[],[],handles.question); %3 blank, then question
        if(delcon==1);
            if(length(index_selected)==length(handles.countedfiles))
                %if all results are selected, treat as 'Clear Data" command
                [handles]=cleardata(hObject, handles);
            else
                for index=1:length(index_selected)
                    handles.fulllist(handles.goodlist(index_selected(index)))=0;  %remove results from lists
                    handles.otherout{handles.goodlist(index_selected(index))}{6}=0; %clear data to save memory
                end
                nobad=find(handles.fulllist~=0);
                handles.goodlist = handles.fulllist(nobad); %create new list of remaining results
                handles.countedfiles=cell(length(handles.goodlist),1);
                for i=1:length(handles.goodlist)
                    handles.countedfiles{i}=handles.otherout{handles.goodlist(i)}{2};
                end
                set(handles.listboxData, 'Value', 1);
                set(handles.listboxData, 'String', handles.countedfiles);
                set(handles.textHelp, 'String', 'Stored results have been deleted.');
                set(handles.uipushtoolHigh, 'Enable', 'off');
                set(handles.uipushtoolFound, 'Enable', 'off');
            end
        end
    else
        %single results was selected, delete only that result
        FileName=char(handles.otherout{handles.goodlist(index_selected)}{1});
        handles.question=['Clear result ', FileName, '?'];
        %send question to confirm_gui, which will display a yes/no window
        delcon = confirm_gui([],[],[],handles.question); %3 blank, then question
        if(delcon==1);
            if(length(handles.goodlist)==1)
                %if only one result exists, treat as a 'Clear Data' command
                [handles]=cleardata(hObject, handles);
            elseif(length(handles.goodlist)>1)
                handles.fulllist(handles.goodlist(index_selected))=0;  %remove result from lists
                handles.otherout{handles.goodlist(index_selected)}{6}=0; %clear data to save memory
                nobad=find(handles.fulllist~=0);
                handles.goodlist = handles.fulllist(nobad); %create new list of remaining results
                handles.countedfiles=cell(length(handles.goodlist),1);
                for i=1:length(handles.goodlist)
                    handles.countedfiles{i}=handles.otherout{handles.goodlist(i)}{2};
                end
                set(handles.listboxData, 'Value', 1);
                set(handles.listboxData, 'String', handles.countedfiles);
                set(handles.textHelp, 'String', 'Stored result has been deleted.');
                set(handles.uipushtoolHigh, 'Enable', 'off');
                set(handles.uipushtoolFound, 'Enable', 'off');
            end
        end
    end
end
guidata(hObject,handles)


% --- Executes on button press in pushbuttonExport.
function pushbuttonExport_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonExport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
names=fieldnames(handles); %make a list of all components within the handles structure
%check if results exist:
if(max(strcmp(names,'goodlist')==1) && ~isempty(handles.goodlist));
%open window to ask whether to save all or only selected results
%savechoice: 1 = export all results ; 2 = export selected results
savechoice=colony_export;
if(savechoice~=0) %chek if user canceled out of save
    [FileName,PathName,FilterIndex] =uiputfile({'*.xlsx','MS Excel File (*.xlsx)';'*.txt','Text File - tab delimited (*.txt)'},'Export Results','*.xlsx');
    if(FilterIndex~=0)
        [pathstr, name, ext] = fileparts(FileName);
        switch lower(ext)
        case '.xlsx' % saving as excel file
            sheetout = colony_sheetname([],[],[],PathName, FileName); %ask user for an Excel sheetname
            sheetname=sheetout{1};
            cd(PathName); %go to new directory
            goahead=sheetout{2};
            if(goahead==1) %user did not cancel out of sheetname option
                [writeoutput]=buildout(hObject, handles, savechoice); %build compatible output format
                status=xlswrite(FileName, writeoutput, sheetname); %write to excel file
                if(status==1)
                    set(handles.textHelp, 'String', 'Exported Data to Excel Spreadsheet.');
                else
                    set(handles.textHelp, 'String', 'Error in Excel export. Is the file open? Is Excel installed?');
                end
            else
                set(handles.textHelp, 'String', 'Export canceled.');
            end
            set(handles.figure1, 'Pointer', 'arrow');
            pause(0.01)
        case '.txt' % saving as text file
            set(handles.figure1, 'Pointer', 'watch');
            pause(0.01);
            cd(PathName); %go to new directory
            [writeoutput]=buildout(hObject, handles, savechoice); %build compatible output format
            fid = fopen(FileName, 'wt'); %open new text file, overwriting an existing one if needed
            for i=1:size(writeoutput,1)
                for j=1:size(writeoutput,2)
                    %write tab delimited line of data
                    if(j<size(writeoutput,2)) %not last column
                        if(iscellstr(writeoutput(i,j))==0)
                            fprintf(fid, '%-1.0f \t', writeoutput{i,j}); %cell is number
                        else
                            fprintf(fid, '%-1s \t', writeoutput{i,j}); %cell is string
                        end
                    %once hit end of line, include line return for new line
                    else
                        if(iscellstr(writeoutput(i,j))==0)
                            fprintf(fid, '%-1.0f \n', writeoutput{i,j}); %cell is number
                        else
                            fprintf(fid, '%-1s \n', writeoutput{i,j}); %cell is string
                        end
                    end
                end
            end
            fclose(fid);
            set(handles.figure1, 'Pointer', 'arrow');
            pause(0.01);
        end
    else
        set(handles.textHelp, 'String', 'Export canceled.');
    end
else
    set(handles.textHelp, 'String', 'Export canceled.');
end
end
guidata(hObject,handles);
set(handles.figure1, 'Pointer', 'arrow');


% --- buildout executes during pushbuttonExport_Callback.
% --- Creates a structure amenable to exporting as text or Excel files
function [writeoutput]=buildout(hObject, handles, savechoice)
writeoutput=cell(1,12);
writeoutput{1,1}=('Image name');
writeoutput{1,2}=('Row x Col');
writeoutput{1,3}=('ROI ID');
writeoutput{1,4}=('# Colonies');
writeoutput{1,5}=('Mean size (pixels^2)');
writeoutput{1,6}=('Background');
writeoutput{1,7}=('Resolution');
writeoutput{1,8}=('Operator name');
writeoutput{1,9}=('Experiment name');
writeoutput{1,10}=('NICE launched');
writeoutput{1,11}=('Counted time');
writeoutput{1,12}=('Notes');
%savechoice: 1 = export all results ; 2 = export selected results
if(savechoice==1)
    for i=1:length(handles.goodlist)
        writeoutput=cat(1,writeoutput,handles.outputstruc{handles.goodlist(i)});
    end
else
    index_selected = get(handles.listboxData,'Value');
    for i=1:length(index_selected)
        writeoutput=cat(1,writeoutput,handles.outputstruc{handles.goodlist(index_selected(1,i))});
    end
end


function editrow_Callback(hObject, eventdata, handles)
% hObject    handle to editrow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%Deactivate Move ROI function to prevent errors caused by clicking outside
%of edit box after changing value
[handles]=toolsoff(hObject, handles); %deactivate toolbar icons
set(handles.figure1, 'WindowButtonDownFcn',''); %Disable move ROI function
names=fieldnames(handles); %make a list of all components within the handles structure
if(max(strcmp(names,'counts_string'))==1)
    handles=rmfield(handles, 'counts_string');
    set(handles.uipushtoolHigh, 'Enable', 'off');  %disable Highlight image button
end
subrow=str2double(get(hObject, 'String')); %get the typed value
subrow=isgoodnum(subrow,2); %determine if the value is allowed, if not then change
set(handles.editrow, 'String', num2str(subrow)); %keep the new value
handles.subrow = subrow;  % Save the new value
handles.activeROI=[1 1];  %set the first ROI as the active ROI
[handles]=fastroi(hObject, handles); %calculate new ROIs
[handles]=plotsubIrect(hObject, handles); %plot image with ROIs
%set(handles.figure1, 'WindowButtonDownFcn',@roibuttondown); % Reenable move ROI function
set(handles.figure1, 'WindowButtonDownFcn',@LJBtesting); % Reenable move ROI function
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function editrow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editrow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editcol_Callback(hObject, eventdata, handles)
% hObject    handle to editcol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of editcol as text
%        str2double(get(hObject,'String')) returns contents of editcol as a double
%Deactivate Move ROI function to prevent errors caused by clicking outside
%of edit box after changing value
[handles]=toolsoff(hObject, handles); %deactivate toolbar icons
set(handles.figure1, 'WindowButtonDownFcn',''); %Disable move ROI function
names=fieldnames(handles); %make a list of all components within the handles structure
if(max(strcmp(names,'counts_string'))==1)
    handles=rmfield(handles, 'counts_string');
    set(handles.uipushtoolHigh, 'Enable', 'off');  %disable Highlight image button
end
subcol=str2double(get(hObject, 'String')); %get the typed value
subcol=isgoodnum(subcol, 2); %determine if the value is allowed, if not then change
set(handles.editcol, 'String', num2str(subcol)); %keep the new value
handles.subcol = subcol;% Save the new value
handles.activeROI=[1 1];  %set the first ROI as the active ROI
[handles]=fastroi(hObject, handles); %calculate new ROIs
[handles]=plotsubIrect(hObject, handles); %plot image with ROIs
%set(handles.figure1, 'WindowButtonDownFcn',@roibuttondown); % Reenable move ROI function
set(handles.figure1, 'WindowButtonDownFcn',@LJBtesting); % Reenable move ROI function
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function editcol_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editcol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- fastroi is called in order to make a grid of ROIs (regions of interest)
% --- It uses the image size or a parent ROI, along with the rows and
%     columns of ROIS to make the grid
function [handles]=fastroi(hObject, handles)
if(handles.parentroi==1)
    %if parent ROI box is checked, determine if values for parent exist.
    %if not, then generate new parent using the size of the image
    if(isempty(handles.parentrect) || handles.parentrect(4)==0 || handles.parentrect(3)==0); %needed for batch check
        handles.parentrect=[1 1 size(handles.I,2)-1 size(handles.I,1)-1];
    end
    if(strcmp(handles.ROI_parentshape, 'rectangle'))
        %get the size of the image for later calculation
        Ihigh=handles.parentrect(4);
        Iwide=handles.parentrect(3);
        handles.offsetx = handles.parentrect(1);
        handles.offsety = handles.parentrect(2);
    elseif(strcmp(handles.ROI_parentshape, 'ellipse'))
        if(handles.subcol==1 && handles.subrow==1 && strcmp(handles.ROI_shape, 'ellipse'))
        %if only one indiv elliptical ROI exists, use entire parent size
            Ihigh=handles.parentrect(4);
            Iwide=handles.parentrect(3);
            handles.offsetx = handles.parentrect(1);
            handles.offsety = handles.parentrect(2);
        else
        %multiple ROIs (or one rectang ROI) within elliptical parent need to be squeezed
            Rtheta=1/sqrt(2);
            handles.offsetx=handles.parentrect(1)+(handles.parentrect(3)/2)-(handles.parentrect(3)/2)*cos(Rtheta);
            handles.offsety=handles.parentrect(2)+(handles.parentrect(4)/2)-(handles.parentrect(4)/2)*sin(Rtheta);
            Iwide=cos(Rtheta)*handles.parentrect(3);
            Ihigh=sin(Rtheta)*handles.parentrect(4);
        end
    else
        errordlg('Unable to determine Parent ROI shape during fastroi.','ROI error', 'modal')
    end
else
    Ihigh=size(handles.I,1);
    Iwide=size(handles.I,2);
    handles.offsetx = 1;
    handles.offsety = 1;
end
handles.xwide = floor(Iwide/handles.subcol);
handles.ywide = floor(Ihigh/handles.subrow);
handles.subIrect=cell(handles.subrow,handles.subcol);
%generate the ROI grid
for scol=1:handles.subcol
    for srow=1:handles.subrow
        handles.subIrect{srow,scol}=[handles.offsetx+(scol-1)*handles.xwide handles.offsety+(srow-1)*handles.ywide handles.xwide-1 handles.ywide-1];
    end
end
if(handles.batch==1)
    handles.roistruc{handles.filenum,1}=handles.subIrect; %save ROI for batch file
end
set(handles.uitoggletoolDrag, 'Enable', 'on') %allow ROIs to be dragged by mouse
guidata(hObject,handles)

% --- threshroi uses simple threshold funciton
% --- used currently for colony sizing, and is called during count function
function [handles]=threshroi(hObject, handles)
%determine if parent ROI is in use
if(handles.parentroi==1);
%     I=imcrop(handles.I,handles.parentrect);
    level = graythresh(imcrop(handles.I,handles.parentrect)); %find gray threshold using Otsu method
else
%     I=handles.I;
    level = graythresh(handles.I); %find gray threshold using Otsu method
end
% level = graythresh(I); %find gray threshold using Otsu method
%create a thresholded image for use by count function
% handles.BWthresh=imcomplement(im2bw(I,level));
handles.BWthresh=imcomplement(im2bw(handles.I,level));
guidata(hObject,handles)

% --- plotsubIrect plots the ROI rectangles and is called by many functions
% --- It replots the entire image, and can be used to display processed
%     or colony highlighted images
function [handles]=plotsubIrect(hObject, handles, PlotType)
if(nargin<3) %if less than 3 input arguements, plot a 'regular' image
    PlotType='normal';  %not Highlight or Found
end    
set(handles.figure1, 'Pointer', 'watch');
pause(0.01);
names=fieldnames(handles); %make a list of all components within the handles structure
%Determine whether to display a processed (found) image.
if(strcmp(PlotType,'Found'))
    axes(handles.axes1), image(handles.BWfinal) %display BW processed image
    colormap([0 0 0; 1 1 1]) %force BW colormap
else
    axes(handles.axes1), image(handles.I)  %display original image
    colormap(gray(256))  %use 256 grayscale colors for display
end
axis off          % Remove axis ticks and numbers
axis image        % Set aspect ratio to obtain square pixels
hold on
if(handles.parentroi==1);
    if(max(handles.parentrect)==0)
        %if user has results with and without parent ROIs, need to have
        %catch in case they try to open non-parented image.
        handles.parentrect=[1 1 size(handles.I,2)-1 size(handles.I,1)-1];
    end
    %draw the parent ROI
    [handles]=drawROI(hObject, handles, 0, 'new',[],[]);
else
end
%Check to make sure parent resizing is not still in effect
%If it is, force back to regular move roi
if(handles.parentresizing==1)
    [handles]=toolsoff(hObject, handles); %deactivate toolbar icons
    handles.parentresizing=0; %parent is no longer being resized
%    set(handles.figure1, 'WindowButtonDownFcn',@roibuttondown); %move ROI function
    set(handles.figure1, 'WindowButtonDownFcn',@LJBtesting); %move ROI function
    set(handles.figure1,'WindowButtonMotionFcn','')
    set(handles.figure1,'WindowButtonUpFcn','')
    set(handles.textHelp, 'String', 'Parent ROI has been resized');
    pause(0.01);
end
%Display the ROI rectangles
for scol=1:handles.subcol
    for srow=1:handles.subrow
        [handles]=drawROI(hObject, handles, [srow scol], 'new',[],[]);
    end
end
%Display the counts within ROIs (if they exist)
if(max(strcmp(names,'counts_string'))==1)
    for scol=1:handles.subcol
        for srow=1:handles.subrow
            text(handles.subIrect{srow,scol}(1)+5,handles.subIrect{srow,scol}(2)+40,handles.counts_string{srow,scol},'Color','b','FontSize',12,'FontWeight','bold');
        end
    end
end
%Display marker over each counted colony center, if creating a "Highlighted" image
if(strcmp(PlotType,'Highlight'))
    for scol=1:handles.subcol
        for srow=1:handles.subrow
            if(~isempty(handles.centroids{srow,scol}))
                for n=1:(size(handles.centroids{srow,scol},1))
                    plot(handles.centroids{srow,scol}(n,1)+handles.subIrect{srow,scol}(1),handles.centroids{srow,scol}(n,2)+handles.subIrect{srow,scol}(2),'r+');
                end
            end
        end
    end
end
%use renamed posRect2Vertices function to get roi_rectangle vertices
Roi_vert = getRectVertices(handles.subIrect{handles.activeROI(1), handles.activeROI(2)});
%plot corners (which can later be used to resize the ROI)
handles.corner(1)=scatter(Roi_vert(1,1),Roi_vert(1,2),'bs');
handles.corner(2)=scatter(Roi_vert(2,1),Roi_vert(2,2),'bs');
handles.corner(3)=scatter(Roi_vert(3,1),Roi_vert(3,2),'bs');
handles.corner(4)=scatter(Roi_vert(4,1),Roi_vert(4,2),'bs');
hold off
%Create functions to display resizing pointer when cursor over corners
spot1func.traverseFcn = @(figHandle, currentPoint)set(figHandle, 'Pointer', 'topl');
spot2func.traverseFcn = @(figHandle, currentPoint)set(figHandle, 'Pointer', 'botl');
spot3func.traverseFcn = @(figHandle, currentPoint)set(figHandle, 'Pointer', 'botr');
spot4func.traverseFcn = @(figHandle, currentPoint)set(figHandle, 'Pointer', 'topr');
%Create functions to note whether cursor moved onto corner
spot1func.enterFcn = @(figHandle, currentPoint)set(handles.corner(1),'UserData','on');
spot2func.enterFcn = @(figHandle, currentPoint)set(handles.corner(2),'UserData','on');
spot3func.enterFcn = @(figHandle, currentPoint)set(handles.corner(3),'UserData','on');
spot4func.enterFcn = @(figHandle, currentPoint)set(handles.corner(4),'UserData','on');
%Create functions to note whether cursor moved off corner
spot1func.exitFcn = @(figHandle, currentPoint)set(handles.corner(1),'UserData','off');
spot2func.exitFcn = @(figHandle, currentPoint)set(handles.corner(2),'UserData','off');
spot3func.exitFcn = @(figHandle, currentPoint)set(handles.corner(3),'UserData','off');
spot4func.exitFcn = @(figHandle, currentPoint)set(handles.corner(4),'UserData','off');
%turn on pointer monitor for corners
iptSetPointerBehavior(handles.corner(1), spot1func);
iptSetPointerBehavior(handles.corner(2), spot2func);
iptSetPointerBehavior(handles.corner(3), spot3func);
iptSetPointerBehavior(handles.corner(4), spot4func);
iptPointerManager(gcf); %activate pointer manager
handles.activeCorner=0; %no ROI corner is selected
set(handles.figure1, 'Pointer', 'arrow');
guidata(hObject,handles);

% --- Executes on button press in checkboxParentROI.
% --- This turns on (and plots) or off the Parent ROI
function checkboxParentROI_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxParentROI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of checkboxParentROI
if (get(hObject,'Value') == get(hObject,'Max'))  % Checkbox is now checked-take approriate action
    %If parent ROI value does not exist, create one based on image size
    if(isempty(max(handles.parentrect)) || max(handles.parentrect)==0)
        handles.parentrect=[1 1 size(handles.I,2)-1 size(handles.I,1)-1];
    else
    end
    handles.parentroi=1; %parent ROI is on
    set(handles.uipushtoolParent, 'Enable', 'on'); %Turn on Parent ROI resizing toolbar button
else     % Checkbox is not checked-take approriate action
    %turn off parent ROI
    handles.parentroi=0; 
    set(handles.uipushtoolParent, 'Enable', 'off'); %Disabel Parent ROI resizing toolbar button
end
[handles]=plothist(hObject, handles, 'on');
[handles]=fastroi(hObject, handles);
[handles]=plotsubIrect(hObject, handles);
guidata(hObject,handles);


function editbkgd_Callback(hObject, eventdata, handles)
% hObject    handle to editbkgd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of editbkgd as text
%        str2double(get(hObject,'String')) returns contents of editbkgd as a double
bkgd=str2double(get(hObject, 'String')); %get the entered value
bkgd=isgoodnum(bkgd,1); %determine if value is allowed, if not change to valid
set(handles.editbkgd, 'String', num2str(bkgd)); %use new value
handles.bkgd = bkgd;  % Save the new value
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function editbkgd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editbkgd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenuResolution.
% --- Resolution is used to determine the amount of blurring, etc done in
%     precounting processing
function popupmenuResolution_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuResolution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = get(hObject,'String') returns popupmenuResolution contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuResolution
val = get(hObject,'Value');
str = get(hObject, 'String');
switch str{val};
case 'Low' % User selects center of low res
    handles.reschoice=1;
case 'Medium' % User selects med res
    handles.reschoice=2;
case 'High' % User selects high res
    handles.reschoice=3;
end
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function popupmenuResolution_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuResolution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on selection change in popupmenuBkgd.
% --- Background sets a threshold for which a colony found by local minima
%     must meet.  Otherwise the count is discarded
function popupmenuBkgd_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuBkgd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = get(hObject,'String') returns popupmenuBkgd contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuBkgd
val = get(hObject,'Value'); 
str = get(hObject, 'String');
switch str{val};
case '-3 sigma' % User selects -3 sigma
	bkgd=handles.bkgdvals(1);
    set(handles.editbkgd, 'Enable', 'off'); %disable manual background box
    handles.bkgdchoice=1;
case '-4 sigma' % User selects -4 sigma
	bkgd=handles.bkgdvals(2);
    set(handles.editbkgd, 'Enable', 'off'); %disable manual background box
    handles.bkgdchoice=2;
case '-5 sigma' % User selects -5 sigma
	bkgd=handles.bkgdvals(3);
    set(handles.editbkgd, 'Enable', 'off'); %disable manual background box
    handles.bkgdchoice=3;
case '-6 sigma' % User selects -2 sigma
	bkgd=handles.bkgdvals(4);
    set(handles.editbkgd, 'Enable', 'off'); %disable manual background box
    handles.bkgdchoice=4;
case 'Otsu' % User selects -3 sigma
	bkgd=handles.bkgdvals(5);
    set(handles.editbkgd, 'Enable', 'off'); %disable manual background box
    handles.bkgdchoice=5;    
case 'Manual' % User selects manual
	bkgd=str2double(get(handles.editbkgd, 'String'));
    set(handles.editbkgd, 'Enable', 'on'); %enable manual background box
    handles.bkgdchoice=6;
end
handles.bkgd = bkgd;
set(handles.editbkgd, 'String', bkgd);
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function popupmenuBkgd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuBkgd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbuttonCount.
function pushbuttonCount_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonCount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tic %start the timer
set(handles.pushbuttonCount, 'BackgroundColor', [1 1 0]); %change count button to yellow
set(handles.figure1, 'Pointer', 'watch');
set(handles.textHelp, 'String', 'Counting...');
pause(0.01);
if(handles.batch==0) %single file mode
    if(handles.parentresizing==1)
        %if the parent was left active, a new histogram is needed
        [handles]=plothist(hObject, handles, 'on');
    end
    [handles]=pushbuttonTabResults_Callback(handles.figure1, [], handles); %change listbox to Results tab
    [handles]=count(hObject, handles); %count colonies
    guidata(hObject,handles)
    set(handles.pushbuttonCount, 'BackgroundColor', [0 1 0]); %change button to green
    pause(0.01);
    toctime=toc; %get timer value
    tocstring=['Step time: ',sprintf('%2.0f',toctime), ' sec']; %convert to string
    set(handles.texttimetoc, 'String', tocstring); %display time
elseif(handles.batch==1) %batch mode
    %save the current ROIs before couting begins
    if(handles.parentroi==1);
        handles.roistruc{handles.filenum,2}=handles.parentrect;
        lastparent=handles.parentrect; %keep the last parent in case a parent is missing
    else
    end
    handles.roistruc{handles.filenum,1}=handles.subIrect;
    [handles]=pushbuttonTabResults_Callback(handles.figure1, [], handles); %change listbox to Results tab
    %Go to the next image
    for Nfile=1:length(handles.FileNames)
        if (handles.filecount==handles.countcount);
            handles.filecount=handles.filecount+1; %index for number of files opened & counted after program launch
        else
            handles.filecount=handles.filecount; %index for number of files opened after program launch
        end
        handles.filenum=Nfile; %this prevents an ROI error if recount immediately
        handles.FileName=handles.FileNames{Nfile}; %get the current filename
        batch_string=['Batch File ', num2str(Nfile), ' of ', num2str(length(handles.FileNames))];
        set(handles.textBatch, 'string', batch_string);
        [handles]=fileopen(hObject, handles);
        filecount_string = sprintf('%03.0f',handles.filecount);  %convert num to string
        handles.numbered_filename=([filecount_string, '-',handles.FileNamenoext]);
        [handles]=plothist(hObject, handles, 'off'); %plot histogram with or without lines on/off line plots
        pause(0.01);
        %Load ROI structure for this image
        handles.subIrect=handles.roistruc{Nfile,1};
        handles.parentrect=handles.roistruc{Nfile,2};
        %determine if parent ROI is needed and exists
        if(handles.parentroi==1 && isempty(handles.parentrect))
            handles.parentrect=lastparent; %use last parent
            [handles]=fastroi(hObject, handles);
        end
        %if an image lacks ROIs, create them before counting
        if(isempty(handles.subIrect)==1)
            [handles]=fastroi(hObject, handles);
            [handles]=count(hObject, handles); %count colonies
        else
            [handles]=count(hObject, handles); %count colonies
        end
        toctime=toc; %get timer value
        tocstring=['Step time: ',sprintf('%2.0f',toctime), ' sec']; %convert to string
        set(handles.texttimetoc, 'String', tocstring); %display time
        guidata(hObject,handles)
        pause(0.01);
    end
    set(handles.pushbuttonCount, 'BackgroundColor', [0 1 0]); %change Count button to green
else
    errordlg('Cannot determine if single file or batch','Count Error', 'modal')
    set(handles.pushbuttonCount, 'BackgroundColor', [0 1 0]); %change button to green
    set(handles.figure1, 'Pointer', 'arrow');
    set(handles.textHelp, 'String', 'Error occured in counting.');
end
set(handles.uipushtoolFound, 'Enable', 'on');
set(handles.uipushtoolHigh, 'Enable', 'on');
set(handles.textHelp, 'String', 'Finished counting.');
set(handles.figure1, 'Pointer', 'arrow');
guidata(hObject,handles)


% --------------------------------------------------------------------
function [handles]=count(hObject, handles)
set(handles.figure1, 'Pointer', 'watch');
pause(0.01)
%check if the loaded ROIs match the most recently used values for rows and
%columns.  If not, recalculate ROIs.
if(handles.subrow~=size(handles.subIrect,1))
    errordlg('Stored and set ROI #s do not match.  Will recalculate ROIs.','Bad ROIs','modal')
    [handles]=fastroi(hObject, handles);
elseif(handles.subcol~=size(handles.subIrect,2))
    errordlg('Stored and set ROI #s do not match.  Will recalculate ROIs.','Bad ROIs','modal')
    [handles]=fastroi(hObject, handles);
end
%create variables to hold data
data=cell(handles.subrow,handles.subcol);
labeledsub=cell(handles.subrow,handles.subcol);
colonycounts=zeros(handles.subrow,handles.subcol);  %only dark objects (below threshold)
meansize=cell(handles.subrow,handles.subcol);
counts_string=cell(handles.subrow,handles.subcol); 
resop=handles.resop;
reschoice=handles.reschoice;
%blur the image to remove pixel noise
PSF = fspecial('gaussian',resop(reschoice,1),resop(reschoice,2)); %5 5 =1200   3,5=600
BWfinal = imfilter(handles.I,PSF,'symmetric','conv');
%create dilation filters
se90 = strel('line', 2, 90); 
se0 = strel('line', 2, 0);
%find local minima (will dilate later)
BWfinal=imextendedmin(BWfinal,resop(reschoice,3),resop(reschoice,4));
%go through each ROI and determine if each count is valid
for scol=1:handles.subcol
    for srow=1:handles.subrow
        if(strcmp(handles.ROI_shape, 'rectangle'))
            %Crop the BW image and remove objects touching the edge 
            subBW=imclearborder(imcrop(BWfinal, handles.subIrect{srow,scol}),8);
        elseif(strcmp(handles.ROI_shape, 'ellipse'))
            subBW=imcrop(BWfinal, handles.subIrect{srow,scol});
            centerpointx=handles.subIrect{srow,scol}(4)/2;
            centerpointy=handles.subIrect{srow,scol}(3)/2;
            for y=1:handles.subIrect{srow,scol}(3)
                for x=1:handles.subIrect{srow,scol}(4)
                    if(((x-centerpointx)^2/centerpointx^2+(y-centerpointy)^2/centerpointy^2)>1)
                        subBW(x,y)=1;
                    end
                end
            end
            subBW=imclearborder(subBW,8);
        else
            errordlg('Unable to determine ROI shape during count.','ROI error', 'modal')
        end
        %Crop the regular image for use in intensity comparison
        Itemp=imcrop(handles.I,handles.subIrect{srow,scol});
        %label all BW objects
        [labeledsub{srow,scol},numObjectsinit] = bwlabel(subBW,8); % 8-connected 
        %dilate the labeled image
        %this allows larger mean intensity area without merging colonies
        labeledsub{srow,scol}=imdilate(labeledsub{srow,scol},[se90 se0]);
        %gather information about the objects
        data{srow,scol} = regionprops(labeledsub{srow,scol},Itemp,'Area','PixelIdxList', 'Eccentricity', 'Centroid', 'MeanIntensity');
        %determine is this is a valid colony
        for n=1:size(data{srow,scol},1);
            %is the colony absurdly large?
            if(data{srow,scol}(n).Area>=2000); 
                BWfinal(data{srow,scol}(n).PixelIdxList)=0;
                data{srow,scol}(n).good=0;
            %is the colony mean darkness below the set background threshold
            elseif(data{srow,scol}(n).MeanIntensity>=handles.bkgd)
                BWfinal(data{srow,scol}(n).PixelIdxList)=0;
                data{srow,scol}(n).good=0;
            %is the colony extremely linear (i.e., scratch or dish wall)
            elseif(data{srow,scol}(n).Eccentricity>=0.98)
                BWfinal(data{srow,scol}(n).PixelIdxList)=0;
                data{srow,scol}(n).good=0;
            %otherwise count it as a good colony
            else
                colonycounts(srow,scol)=colonycounts(srow,scol)+1;
                data{srow,scol}(n).good=1;
            end
        end
        %generate a cell of the ROI counts for later display
        counts_string{srow,scol}=sprintf('%2.0f',colonycounts(srow,scol));  %need string for text overlay
    end
end
%if Colony Sizes is checked, determine them
%This sizing is a rough estimation, performed by thresholding
%Size is reported as area in units of pixels
if(handles.sizeon==1)
    [handles]=threshroi(hObject, handles); %generate a thresholded image
    for scol=1:handles.subcol
        for srow=1:handles.subrow
            subthresh=imclearborder(imcrop(handles.BWthresh, handles.subIrect{srow,scol}),8);
            sizepix=sum(sum(subthresh)); %add everything together
            %divide by the total number of colonies
            if(colonycounts(srow,scol)==0)
                meansize{srow,scol}='';
            else
                meansize{srow,scol}=sprintf('%2.2f',sizepix./colonycounts(srow,scol));
            end
        end
    end
end
%prepare output structure
if (handles.filecount==handles.countcount);
    handles.countcount=handles.countcount; %index for number of files opened & counted after program launch
else
    handles.countcount=handles.countcount+1; %index for number of files opened after program launch
end
handles.colonycounts=colonycounts;
handles.counts_string=counts_string;
handles.data=data;
countedtime=datestr(fix(clock)); %get the current time
resolution_value = get(handles.popupmenuResolution,'Value');
resolution_string = get(handles.popupmenuResolution, 'String');
resolution=resolution_string{resolution_value};
background_value = get(handles.popupmenuBkgd,'Value');
background_string = get(handles.popupmenuBkgd, 'String');
background=background_string{background_value};
%get the current ROI grid format
rowcol=[sprintf('%2.0f',handles.subrow), 'x', sprintf('%2.0f',handles.subcol)];
outputcell=cell(handles.subrow*handles.subcol,12);
%fill in the details for all ROIs.  This is kept for exporting
posind=0;
for scol=1:handles.subcol
    for srow=1:handles.subrow
        posind=posind+1;
        outputcell{posind,1}=handles.numbered_filename; %numbered filename
        outputcell{posind,2}=rowcol; %#rows x #cols
        outputcell{posind,3}=posind;  %ROI ID
        outputcell{posind,4}=colonycounts(srow,scol);  %count
        outputcell{posind,5}=meansize{srow,scol};  %mean colony size
        outputcell{posind,6}=background;  %background
        outputcell{posind,7}=resolution;  %resolution
        outputcell{posind,8}=handles.expdetails{2}; %operator
        outputcell{posind,9}=handles.expdetails{3}; %experiment
        outputcell{posind,10}=handles.expdetails{1}; %date program launch
        outputcell{posind,11}=countedtime; %date counted
        outputcell{posind,12}=handles.expdetails{4}; %notes
    end
end
handles.outputstruc{handles.countcount}=outputcell;
%create temporary output, used when loading Results
otherout=cell(10,1);
otherout{1}=handles.FileName; %original filename
otherout{2}=handles.numbered_filename; %numbered filename
otherout{3}=handles.subIrect;  %ROI locations
otherout{4}=handles.counts_string; %ROI counts
otherout{5}=handles.parentrect;  %parent ROI info
%generate a column of centroids instead of saving data (which is memory
%intensive)
handles.centroids=cell(handles.subrow,handles.subcol);
for scol=1:handles.subcol
    for srow=1:handles.subrow
        index=0; %index of good counts in ROI
        for n=1:length(handles.data{srow,scol})
            if(handles.data{srow,scol}(n).good==1)
                index=index+1; 
                handles.centroids{srow,scol}(index,1)=handles.data{srow,scol}(n).Centroid(1);
                handles.centroids{srow,scol}(index,2)=handles.data{srow,scol}(n).Centroid(2);
            end
        end
    end
end
otherout{6}=handles.centroids;  %save colony locations
otherout{7}={handles.ROI_shape, handles.ROI_parentshape}; %ROI shapes
otherout{8}=handles.PathName; %pathname
otherout{9}=handles.invert;  %1 if image contrast was inverted
otherout{10}=0; %empty
handles.otherout{handles.countcount}=otherout;
handles.fulllist(handles.countcount)=handles.countcount; %list of all files
nobad=find(handles.fulllist~=0);
handles.goodlist = handles.fulllist(nobad); %update the list of good files (not deleted)
%generate a list of all (good) files for display in the listbox
handles.countedfiles=cell(length(handles.goodlist),1);
for i=1:length(handles.goodlist)
    handles.countedfiles{i}=handles.otherout{handles.goodlist(i)}{2}; %get the numbered filename
end
set(handles.listboxData, 'String', handles.countedfiles); %create the listbox
set(handles.listboxData, 'Value', length(handles.countedfiles)); %highlight the last value
handles.BWfinal=BWfinal; %save the current BW processed "found" image
[handles]=plotsubIrect(hObject, handles); %display the counted image
guidata(hObject,handles)


% --- Executes on button press in checkboxSize.
% --- If selected, the mean colony size will be found during the count
%     function
function checkboxSize_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of checkboxSize
if (get(hObject,'Value') == get(hObject,'Max'))
   handles.sizeon=1; % Checkbox is checked-take approriate action, count
else
   handles.sizeon=0; % Checkbox is not checked-take approriate action, no count
end
guidata(hObject,handles);

% --- Executes on button press in checkboxLog.
% --- set histogram y-axis to logarithmic scale
function checkboxLog_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxLog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of checkboxLog
if (get(hObject,'Value') == get(hObject,'Max'))
   handles.histlog=1; % Checkbox is checked-take approriate action, use log-scale
   [handles]=plothist(hObject, handles, 'on');
else
   handles.histlog=0; % Checkbox is not checked-take approriate action, use regular scale
   [handles]=plothist(hObject, handles, 'on');
end
guidata(hObject,handles);

% --- isgoodnum is called by several functions to determine if a typed
%     value is valid
function newvalue=isgoodnum(invalue,param)
if(param==2)  %ROI col/row must be real positive integer
    if(isnan(invalue) || invalue<=0 || ~isreal(invalue) || rem(invalue,1)~=0)
        errordlg('ROI values must be a positive integers','Bad Input','modal')
        newvalue=1; %value is bad, set to 1 instead
    elseif(invalue>25)
        errordlg('ROI rows and columns are limited to 25','Bad Input','modal')
        newvalue=25;  %value is bad, set to 25 instead
    else
        newvalue=invalue; %value is good
    end
elseif(param==1)  %background must be real positive number
    if(isnan(invalue) || invalue<=0 || ~isreal(invalue))
        errordlg('Value must be a positive real number','Bad Input','modal')
        newvalue=100; %value is bad, set to 100 instead
    else
        newvalue=invalue;
    end
end

% --- plothist is called when an image is opened
% --- The lines input determines whether to draw fit, etc.
function [handles]=plothist(hObject, handles, lines)
%if a parent ROI exists, only the pixels within it are used for the histogram
if(handles.parentroi==0 || isempty(imcrop(handles.I, handles.parentrect)));
    [H,xbins] = imhist(handles.I);
elseif(strcmp(handles.ROI_parentshape,'rectangle'))
    %for rectangular parent, use imcrop
    [H,xbins] = imhist(imcrop(handles.I, handles.parentrect));
else
    %for elliptical parent, crop and calculate elliptical mask
    temphist=imcrop(handles.I, handles.parentrect);
    ellimask=false(size(temphist));
    centerpointx=handles.parentrect(4)/2;
    centerpointy=handles.parentrect(3)/2;
    for y=1:size(temphist,1)
        for x=1:size(temphist,2)
            if(((x-centerpointx)^2/centerpointx^2+(y-centerpointy)^2/centerpointy^2)<=1)
                ellimask(x,y)=true;
            end
        end
    end
    [H,xbins] = imhist(temphist(ellimask));
end
if(handles.histlog==0) %use normal y-axis scale
    axes(handles.axesHist),bar(xbins,H), axis tight;
    maxH=max(H);
else
    %use log10 y-axis
    axes(handles.axesHist),bar(xbins,log10(H)), axis tight;
    maxH=log(max(H));
end
%fit the histogram to a Gaussian.
%Since the background dominates, this finds background distribution
Starting=([max(H) 128 25]); %starting parameters for the fit [height center width]
options=optimset('Display','off'); %do not display fitting iterations
%fitting function
Estimates=fminsearch(@fitGaussian,Starting,options,xbins,H);
gray=graythresh(uint8(handles.I))*256; %determine Otsu threshold
%generate the standard deviation background values
bkgdvals=([Estimates(2)-3*abs(Estimates(3)) Estimates(2)-4*abs(Estimates(3)) Estimates(2)-5*abs(Estimates(3)) Estimates(2)-6*abs(Estimates(3)) gray]);
if(strcmp(lines,'on')==1) %check if should plot the lines (not in batch mode)
    hold on
    if(handles.histlog==0)
        plot(xbins,Estimates(1)*exp(-(xbins-Estimates(2)).^2/(2*Estimates(3).^2)), 'b-');
    else
        plot(xbins,log10(Estimates(1)*exp(-(xbins-Estimates(2)).^2/(2*Estimates(3).^2))), 'b-');
    end
    h(1)=line([bkgdvals(1) bkgdvals(1)],[0 maxH],'Color','b'); %center-2sigma
    h(2)=line([bkgdvals(2) bkgdvals(2)],[0 maxH],'Color','g'); %center-3sigma
    h(3)=line([bkgdvals(3) bkgdvals(3)],[0 maxH],'Color','r'); %center-4sigma
    h(4)=line([bkgdvals(4) bkgdvals(4)],[0 maxH],'Color','m'); %center-5sigma
    h(5)=line([bkgdvals(5) bkgdvals(5)],[0 maxH],'Color','c', 'LineStyle', '--'); %Otsu threshold
    legend(h,'3 sigma,fit', '4 sigma','5 sigma','6 sigma','Otsu', 'Location', 'East');
    set(legend, 'FontSize', 5, 'Box', 'off');
    hold off
    axis([0 255 0 maxH]);
    xlabel('Gray Level', 'FontSize', 10, 'fontweight','b');
    ylabel('Population', 'FontSize', 10, 'fontweight','b');
elseif(strcmp(lines,'off')==1)
else
    errordlg('Plothist function only accepts on or off values','Histogram plot error', 'modal')
    set(handles.textHelp, 'String', 'Error calling histogram.');
end
handles.bkgdvals=bkgdvals; %save background values
%check if Manual Background is set.  If not display selected value in box.
if(handles.bkgdchoice==6);
else
    bkgd = bkgdvals(handles.bkgdchoice);
    handles.bkgd=bkgd;
    set(handles.editbkgd, 'String', handles.bkgd);
end
guidata(hObject,handles)

% --- newaxis resizes the image figure window.  Does not execute display.
% --- This is used to optimize the display and prevent problems that can
%     occur if switching before small and large images
function [handles]=newaxis(hObject,handles)
axes(handles.axes1); 
%calculate the magnification
handles.mag=min(min([handles.axispos(4) handles.axispos(3)]./size(handles.I),1)); 
%load original positin
handles.showpos=handles.axispos;
%check image height and width against axis.  Resize axis if needed.
if(handles.axispos(4)-size(handles.I,1)*handles.mag>1)
    handles.showpos(2)=handles.axispos(2)+round(0.5*(handles.axispos(4)-size(handles.I,1)*handles.mag));
end
if(handles.axispos(3)-size(handles.I,2)*handles.mag>1)
    handles.showpos(1)=handles.axispos(1)+round(0.5*(handles.axispos(3)-size(handles.I,2)*handles.mag));
end
handles.showpos=[handles.showpos(1) handles.showpos(2) size(handles.I,2)*handles.mag size(handles.I,1)*handles.mag];
currentaxes=get(handles.axes1, 'Position');
if(currentaxes==handles.showpos) %if axis is same, don't bother redrawing
else
    set(handles.axes1, 'Position', [handles.showpos]);
end
guidata(hObject,handles);


% --------------------------------------------------------------------
function uipushtoolOpen_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtoolOpen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileNames,PathName,FilterIndex] = uigetfile('*.tif;*.jpg','MultiSelect','on');
if(FilterIndex~=0)  %check that user did not cancel out of opening file
    set(handles.figure1, 'Pointer', 'watch');
    pause(0.01);
    handles.filenum=1; %set the image file to 1
    handles.PathName=PathName;
    cd(handles.PathName);
    tempdir=pwd;
    dirname=tempdir(~isspace(tempdir)); %remove all spaces from directory
    set(handles.textforlist,'String',dirname); %display directory without spaces
    [handles]=pushbuttonTabImages_Callback(hObject, [], handles); %make list box display images
    if(~iscellstr(FileNames))  %single file mode (multiple open results in string array)
        handles.FileName=FileNames;
        [handles]=fileopen(hObject, handles, 'yes'); %open file and calculate ROIs
        [handles]=plothist(hObject, handles, 'on'); %plot histogram with or without lines on/off line plots
        if (handles.filecount==handles.countcount);
            handles.filecount=handles.filecount+1; %index for number of files opened & counted after program launch
        else
            handles.filecount=handles.filecount; %index for number of files opened after program launch
        end
        filecount_string = sprintf('%03.0f',handles.filecount);  %need string for text overlay
        handles.numbered_filename=([filecount_string, '-',handles.FileNamenoext]);
        handles.batch=0;  %0 for single file, 1 for batch
        set(handles.textBatch, 'string', 'Single File');
        set(handles.textHelp, 'String', 'Single file loaded.');
    else  %batch file mode
        FileNames=sort(FileNames); %alphabetize filenames
        set(handles.textFilename,'String','Batch Counting')
        handles.FileNames=FileNames;
        handles.roistruc=cell(length(handles.FileNames),2); %create structure to hold ROIs
        handles.FileName=handles.FileNames{1}; %get first filename
        [handles]=fileopen(hObject, handles, 'yes'); %open file and calculate ROIs
        [handles]=plothist(hObject, handles, 'on'); %plot histogram with lines on
        handles.batch=1;  %0 for single file, 1 for batch
        batch_string=['Batch File 1 of ', num2str(length(handles.FileNames))];
        set(handles.textBatch, 'string', batch_string);
        if (handles.filecount==handles.countcount);
            handles.filecount=handles.filecount+1; %index for number of files opened & counted after program launch
        else
            handles.filecount=handles.filecount; %index for number of files opened after program launch
        end
        filecount_string = sprintf('%03.0f',handles.filecount);  %convert num to string
        handles.numbered_filename=([filecount_string, '-',handles.FileNamenoext]);
        set(handles.textHelp, 'String', 'Stack of files loaded.');
    end
    %update the listbox if Images tab is selected
    if(strcmp(handles.tab,'Images')==1)
        set(handles.listboxData, 'Value', 1);
        names=fieldnames(handles); %make a list of all components within the handles structure
        if(handles.batch==1 && max(strcmp(names,'FileNames'))==1 && ~isempty(handles.FileNames))
            listbox_values=handles.FileNames;
        elseif(handles.batch==0 && max(strcmp(names,'FileName'))==1 && ~isempty(handles.FileName))
            listbox_values=handles.FileName;
        else
            listbox_values='no image loaded';
        end
        set(handles.listboxData, 'String', listbox_values);
    end
end
set(handles.figure1, 'Pointer', 'arrow');
guidata(hObject,handles)

% --- fileopen loads current file
% --- Will find ROIs if varagin is 'yes'
function [handles]=fileopen(hObject, handles, varagin)
%1st determine whether to calculate ROI in this function
if nargin==3  %check the number of arguments in calling function
    if(strcmp(varagin,'no')==1)  
        findroi='no';
    else
        findroi='yes';  %if there are 3 arguments, assume yes (unless explicit 'no')
    end
elseif nargin==2
    findroi='no'; %if only 2 arguments, default 3rd value is 'no'
else
    errordlg('Incorrect # arguments loading file','File load error', 'modal')
    set(handles.textHelp, 'String', 'File Load Error #0.');
end
set(handles.figure1, 'Pointer', 'watch');
pause(0.01);
names=fieldnames(handles); %make a list of all components within the handles structure
FileName=handles.FileName;
    if  (FileName==0)
        errordlg('No file selected','File load error', 'modal')
        set(handles.textHelp, 'String', 'File Load Error #1.');
    else
        [path,name,ext] = fileparts(FileName);
        cd(handles.PathName);
        switch lower(ext)
            case {'.tif','.tiff','.jpg','.jpeg'} %check if tif or jpeg
                I=(imread(FileName));
                if(ndims(I)==2) %check if image is grayscale
                    %rescale to 8-bit if higher
                    if(max(max(I))>255)
                        g=double(I);
                        g=(g-min(min(g)))./(max(max(g))-min(min(g)));
                        I=uint8(round(g*255));
                    end
                    handles.I = I;
                    if(handles.invert==1)
                        %invert image contrast
                        handles.I=imcomplement(handles.I);
                    end
                    [handles]=newaxis(hObject,handles);
                    set(handles.uipushtoolFound, 'Enable', 'off');
                    set(handles.uipushtoolHigh, 'Enable', 'off');
                    %if previous counts exist, remove them
                    if(max(strcmp(names,'counts_string'))==1);
                        handles=rmfield(handles, 'counts_string');
                    end
                    handles.activeROI=[1 1];  %set the first ROI as the active ROI
                    %make ROI grid if asked
                    if(strcmp(findroi,'yes')==1)
                        [handles]=fastroi(hObject, handles); %find ROIs
                        [handles]=plotsubIrect(hObject, handles); %display image
                    end
                elseif(ndims(I)==3)  %image may be color
                    if(size(I,3)==3) %assume is RGB color
                        I=rgb2gray(imread(FileName));  % convert to grayscale
                        %rescale to 8-bit if higher
                        if(max(max(I))>255)
                            g=double(I);
                            g=(g-min(min(g)))./(max(max(g))-min(min(g)));
                            I=uint8(round(g*255));
                        end
                        handles.I = I;
                        if(handles.invert==1)
                            %invert image contrast
                            handles.I=imcomplement(handles.I);
                        end
                        [handles]=newaxis(hObject,handles);
                        set(handles.uipushtoolFound, 'Enable', 'off');
                        set(handles.uipushtoolHigh, 'Enable', 'off');
                        %if previous counts exist, remove them
                        if(max(strcmp(names,'counts_string'))==1);
                            handles=rmfield(handles, 'counts_string');
                        end
                        handles.activeROI=[1 1];  %set the first ROI as the active ROI
                        %make ROI grid if asked
                        if(strcmp(findroi,'yes')==1)
                            [handles]=fastroi(hObject, handles); %find ROIs
                            [handles]=plotsubIrect(hObject, handles); %display image
                        end
                    else
                        errordlg('Only 2D or RGB allowed','File load error', 'modal')
                        set(handles.textHelp, 'String', 'File Load Error #2.');
                    end
                else
                    errordlg('Only 2D or RGB allowed','File load error', 'modal')
                    set(handles.textHelp, 'String', 'File Load Error #3.');
                end
            otherwise
                errordlg('Counter currently only reads TIF and JPEG files','File load error', 'modal')
                set(handles.textHelp, 'String', 'File Load Error #4.');
        end
        handles.FileNamenoext=name;
        tempdir=pwd;
        dirname=tempdir(~isspace(tempdir));
        set(handles.textforlist,'String',dirname);
        set(handles.textFilename,'String',FileName)
    end
guidata(hObject,handles)


% --- uipushtoolLogin reloads welcome screen
% --- Any updates will be applied to results from this point onward.
% --- Previous results use the old login details. 
function uipushtoolLogin_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtoolLogin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.expdetails = colony_welcome(handles.expdetails);
guidata(hObject,handles);

% --- uipushtoolSaveImage opens a dialog to save images
% --- User can save image with ROIS, counts, etc., or without these, or
%     can save image of entire NICE window (excluding toolbar)
function uipushtoolSaveImage_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtoolSaveImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
savechoice=colony_imagesave; %open save dialog
if(savechoice~=0) %check if user cancelled out of dialog
    [FileName,PathName,FilterIndex] =uiputfile({'*.jpg','JPEG Files (*.jpg)';'*.tif','TIFF Files (*.tif)'; '*.bmp','Bitmap Files (*.bmp)'},'Save Image of Only (No ROIs, Counts)','*.jpg');
    if(FilterIndex~=0) %check if user cancelled out of save
        if(savechoice==1) %annotated image
            cd(PathName);
            I = getframe(gca);   %save displayed image, zoom, counts
            imwrite(I.cdata, FileName);
            set(handles.textHelp, 'String', 'Saved annotated image.');
        elseif(savechoice==2) %save handles.I
            cd(PathName);
            imwrite(handles.I, FileName);  %save image
            set(handles.textHelp, 'String', 'Saved image file.');
        elseif(savechoice==3) %save handles.I
            cd(PathName);
            I = getframe(gcf);
            imwrite(I.cdata, FileName);  %get NICE window and save
            set(handles.textHelp, 'String', 'Saved image of NICE window.');
        else %do not save
            set(handles.textHelp, 'String', 'Aborted image save.');
        end
    else
        set(handles.textHelp, 'String', 'Aborted image save.');
    end
else
    set(handles.textHelp, 'String', 'Aborted image save.');
end
guidata(hObject,handles);
set(handles.figure1, 'Pointer', 'arrow');


% --- uitoggletoolInvert will cause all images to be displayed with 
%     inverted contrast when active
function uitoggletoolInvert_OnCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletoolInvert (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.figure1, 'Pointer', 'watch');
pause(0.01);
handles.I=imcomplement(handles.I); %invert contrast 
handles.invert=1;
[handles]=plotsubIrect(hObject, handles); %redisplay image
[handles]=plothist(hObject, handles, 'on'); %plot new histogram with lines
guidata(hObject,handles);
set(handles.textHelp, 'String', 'Image contrast inversion is on');
set(handles.figure1, 'Pointer', 'arrow');


% --- This deactivates contrast inversion
function uitoggletoolInvert_OffCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletoolInvert (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.figure1, 'Pointer', 'watch');
pause(0.01);
handles.I=imcomplement(handles.I); %inverst contrast
handles.invert=0;
[handles]=plotsubIrect(hObject, handles); %redisplay image
[handles]=plothist(hObject, handles, 'on'); %plot new histogram with lines
guidata(hObject,handles);
set(handles.textHelp, 'String', 'Image contrast inversion is off');
set(handles.figure1, 'Pointer', 'arrow');

% --- uipushtoolResetImage resets the ROIs of the current image
%     It also forces images to be redisplayed, and resets toolbar icons
function uipushtoolResetImage_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtoolResetImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.figure1, 'Pointer', 'watch');
pause(0.01);
[handles]=toolsoff(hObject, handles); %deactivate toolbar icons
handles.parentrect=[1 1 size(handles.I,2)-1 size(handles.I,1)-1]; %reset parent ROI
[handles]=fileopen(hObject, handles, 'yes'); %this will reset ROIs as well
[handles]=plothist(hObject, handles, 'on'); %plot histogram with or without lines on/off line plots
set(handles.textHelp, 'String', 'Image reset.');
guidata(hObject,handles)
set(handles.figure1, 'Pointer', 'arrow');

% --- uipushtoolParent allows the parent ROI to be resized.
%     The parent ROI checkbox must be checked for the icon to be enabled.
function uipushtoolParent_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtoolParent (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(handles.parentresizing==1) %parent is already being resized
    %stop resizing, calculate ROIs
    [handles]=toolsoff(hObject, handles); %deactivate toolbar icons
    set(handles.figure1,'Pointer','arrow')
%    set(handles.figure1, 'WindowButtonDownFcn',@roibuttondown); %move ROI function
    set(handles.figure1, 'WindowButtonDownFcn',@LJBtesting); %move ROI function
    set(handles.figure1,'WindowButtonMotionFcn','')
    set(handles.figure1,'WindowButtonUpFcn','')
    [handles]=fastroi(hObject, handles); %recalculate ROIs
    [handles]=plotsubIrect(hObject, handles); %redraw image
    [handles]=plothist(hObject, handles, 'on'); %recalculate histogram
    set(handles.textHelp, 'String', 'Parent ROI has been resized');
    %now force to single-click mode to allow repeated selections of Parent ROI
    %(otherwise next selection may immediately deactivate)
    set(gcf,'SelectionType','normal')
    guidata(hObject,handles)
else
    handles.parentresizing=1; %parent is now being resized
    set(handles.textHelp, 'String', 'Drag or resize the parent ROI.  Double-click when finished.');
    [handles]=toolsoff(hObject, handles); %deactivate toolbar icons
     handles.buttonmove=0; %no reoi motion yet
    handles.activeCorner=0; %no ROI corner is selected
    set(handles.figure1, 'Pointer', 'arrow');
    set(handles.figure1, 'WindowButtonDownFcn',''); %Disable move ROI function
    Roi_vert = getRectVertices(handles.parentrect);
    %create corners for parent roi resizing
    axes(handles.axes1),
%     handles.newparent=handles.parentrect;
    [handles]=drawROI(hObject, handles, 0, 'change', [], '--');
    hold on
    handles.parent_corner(1)=scatter(Roi_vert(1,1),Roi_vert(1,2),'ms','SizeData', 120);
    handles.parent_corner(2)=scatter(Roi_vert(2,1),Roi_vert(2,2),'ms','SizeData', 120);
    handles.parent_corner(3)=scatter(Roi_vert(3,1),Roi_vert(3,2),'ms','SizeData', 120);
    handles.parent_corner(4)=scatter(Roi_vert(4,1),Roi_vert(4,2),'ms','SizeData', 120);
    hold off
    %Create functions to display resizing pointer when cursor over corners
    spot1func.traverseFcn = @(figHandle, currentPoint)set(figHandle, 'Pointer', 'topl');
    spot2func.traverseFcn = @(figHandle, currentPoint)set(figHandle, 'Pointer', 'botl');
    spot3func.traverseFcn = @(figHandle, currentPoint)set(figHandle, 'Pointer', 'botr');
    spot4func.traverseFcn = @(figHandle, currentPoint)set(figHandle, 'Pointer', 'topr');
    %Create functions to note whether cursor moved onto corner
    spot1func.enterFcn = @(figHandle, currentPoint)set(handles.parent_corner(1),'UserData','on');
    spot2func.enterFcn = @(figHandle, currentPoint)set(handles.parent_corner(2),'UserData','on');
    spot3func.enterFcn = @(figHandle, currentPoint)set(handles.parent_corner(3),'UserData','on');
    spot4func.enterFcn = @(figHandle, currentPoint)set(handles.parent_corner(4),'UserData','on');
    %Create functions to note whether cursor moved off corner
    spot1func.exitFcn = @(figHandle, currentPoint)set(handles.parent_corner(1),'UserData','off');
    spot2func.exitFcn = @(figHandle, currentPoint)set(handles.parent_corner(2),'UserData','off');
    spot3func.exitFcn = @(figHandle, currentPoint)set(handles.parent_corner(3),'UserData','off');
    spot4func.exitFcn = @(figHandle, currentPoint)set(handles.parent_corner(4),'UserData','off');
    %turn on pointer monitor for corners
    iptSetPointerBehavior(handles.parent_corner(1), spot1func);
    iptSetPointerBehavior(handles.parent_corner(2), spot2func);
    iptSetPointerBehavior(handles.parent_corner(3), spot3func);
    iptSetPointerBehavior(handles.parent_corner(4), spot4func);
    iptPointerManager(gcf); %activate pointer manager
    set(handles.figure1, 'WindowButtonDownFcn',@parentroi_buttondown); % Reenable ROI function
end
guidata(hObject,handles)
pause(0.01);
    
% --- Display the image with counts and ROIs
function [handles]=uipushtoolOrig_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtoolOrig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles]=plotsubIrect(hObject, handles);

% --- Display the processed "found" image with counts and ROIs
function uipushtoolFound_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtoolFound (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles]=plotsubIrect(hObject, handles,'Found');

% --- Display the image with counts and ROIs and markers over every counted
%     colony
function uipushtoolHigh_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtoolHigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles]=plotsubIrect(hObject, handles,'Highlight');


% --- Executes on button press in pushbuttonTabImages.
% --- This changes what is displayed in the listbox
function [handles]=pushbuttonTabImages_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonTabImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%change tab button colors, inactivate current tab button
set(handles.pushbuttonTabImages, 'FontWeight', 'b', 'BackgroundColor', [0.925 0.914 0.847], 'ForegroundColor', [0 0 0], 'Enable', 'Inactive');
set(handles.pushbuttonTabResults, 'FontWeight', 'n', 'BackgroundColor', [0.8 0.8 0.8],'ForegroundColor', [0.4 0.4 0.4], 'Enable', 'on');
%buttons below listbox are not allowed when in images tab
set(handles.pushbuttonExport, 'Enable', 'off');
set(handles.pushbuttonDelete, 'Enable', 'off');
set(handles.pushbuttonCleardata, 'Enable', 'off');
handles.tab='Images';
%select the first value in the listbox (prevents errors)
set(handles.listboxData, 'Value', 1);
names=fieldnames(handles); %make a list of all components within the handles structure
%determine what to display in listbox
if(handles.batch==1 && max(strcmp(names,'FileNames'))==1 && ~isempty(handles.FileNames))
    listbox_values=handles.FileNames;
elseif(handles.batch==0 && max(strcmp(names,'FileName'))==1 && ~isempty(handles.FileName))
    listbox_values=handles.FileName;
else
    listbox_values='no image loaded';
end
%update listbox
set(handles.listboxData, 'String', listbox_values);
guidata(hObject,handles);  % Update handles structure


% --- Executes on button press in pushbuttonTabResults.
% --- This changes what is displayed in the listbox
function [handles]=pushbuttonTabResults_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonTabResults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%change tab button colors, inactivate current tab button
set(handles.pushbuttonTabResults, 'FontWeight', 'b', 'BackgroundColor', [0.925 0.914 0.847], 'ForegroundColor', [0 0 0], 'Enable', 'Inactive');
set(handles.pushbuttonTabImages, 'FontWeight', 'n', 'BackgroundColor', [0.8 0.8 0.8],'ForegroundColor', [0.4 0.4 0.4], 'Enable', 'on');
%enable buttons below listbox
set(handles.pushbuttonExport, 'Enable', 'on');
set(handles.pushbuttonDelete, 'Enable', 'on');
set(handles.pushbuttonCleardata, 'Enable', 'on');
handles.tab='Results';
%select the first value in the listbox (prevents errors)
set(handles.listboxData, 'Value', 1);
handles.filenum=1;
names=fieldnames(handles); %make a list of all components within the handles structure
%determine what to display in listbox
if(max(strcmp(names,'countedfiles'))==1 && ~isempty(handles.countedfiles{1}))
    listbox_values=handles.countedfiles;
else
    listbox_values='cleared';
end
%update listbox
set(handles.listboxData, 'String', listbox_values);
guidata(hObject,handles);  % Update handles structure

% --- uitoggletoolDrag changes the drag one ROI function to a drag all
%     function
function uitoggletoolDrag_OnCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletoolDrag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles]=toolsoff(hObject, handles); %deactivate any tool bar icons
set(handles.textHelp, 'String', 'Left-click drags all ROIs. Right- or Ctrl-Click drags columns.  Middle- or Shift-Click drags rows.');
set(handles.figure1, 'WindowButtonDownFcn',@dragbuttondown); %set new button down function
guidata(hObject,handles);

% --------------------------------------------------------------------
function uitoggletoolDrag_OffCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletoolDrag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles]=toolsoff(hObject, handles); %deactivate tool bar icons
set(handles.uitoggletoolDrag, 'State', 'off')
set(handles.textHelp, 'String', 'Left-click drag single ROI. Right- or Ctrl-Click drags columns.  Middle- or Shift-Click drags rows.');
%set(handles.figure1, 'WindowButtonDownFcn',@roibuttondown); %go back to single drag ROI
set(handles.figure1, 'WindowButtonDownFcn',@LJBtesting); %go back to single drag ROI
set(handles.figure1,'WindowButtonMotionFcn','')
set(handles.figure1,'WindowButtonUpFcn','')
set(handles.figure1,'Pointer','arrow')
guidata(hObject,handles);

% --- toolsoff is called to deactivate any toolbar icons
% --- It also forces the zoom, pan and cursor states to off
% --- This prevents errors that could occur by having multiple states on.
function [handles]=toolsoff(hObject, handles)
%first deactivate the toolbar buttons
set(handles.uitoggletoolZoomIn, 'State', 'off');
set(handles.uitoggletoolZoomOut, 'State', 'off');
set(handles.uitoggletoolPan, 'State', 'off');
set(handles.uitoggletoolDataCursor, 'State', 'off');
%now force the modes themselves to be off
zoom off
pan off
datacursormode off
guidata(hObject,handles)

% --------------------------------------------------------------------
function Helpmenu_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to Helpmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Loads the About window
function About_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to About (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
colony_about


% --------------------------------------------------------------------
function Tutorial_Callback(hObject, eventdata, handles)
% hObject    handle to Tutorial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
colony_tutorial


%-------------------
%set of functions to move single ROI
% ALL references to this function have been removed LJB Feb 2017
%-------------------
function roibuttondown(hObject, handles)
handles = guidata(hObject); %load handles
buttontype=get(handles.figure1,'SelectionType');
%capture current cursor location
cp = get(handles.axes1,'CurrentPoint');
handles.xinit = cp(1,1);
handles.yinit = cp(1,2);
grabbed_edge=false; %grabbed_edge refers to resizing handles
%check the resizing handles to see if they have been selected
for cornnum=1:4
    if(strcmp(get(handles.corner(cornnum),'UserData'),'on'))
        grabbed_edge=true;
        handles.activeCorner=cornnum; %get the active corner
        set(gcf,'WindowButtonMotionFcn',@resize_roimotion) %change the function used when mouse moves
        set(gcf,'WindowButtonUpFcn',@resize_roiup) %change what happens when button released
    end
end
if(~grabbed_edge); %no ROI corner is selected
    %make holder variables
    handles.current_col=[];
    handles.current_row=[];
    handles.newrect=cell(size(handles.subIrect));
    handles.movingrois=false(handles.subrow,handles.subcol);
    handles.buttonmove=0; %check if button motion happened
    %determine if button press occurred inside an ROI
    for scol=1:handles.subcol
        for srow=1:handles.subrow
            if(cp(1,1)>=handles.subIrect{srow,scol}(1) && cp(1,1)<=handles.subIrect{srow,scol}(1)+handles.subIrect{srow,scol}(3) && cp(1,2)>=handles.subIrect{srow,scol}(2) && cp(1,2)<=handles.subIrect{srow,scol}(2)+handles.subIrect{srow,scol}(4))
                handles.current_col=scol;
                handles.current_row=srow;
                handles.activeROI=[srow scol]; %get the active ROI
            end
        end
    end
    handles.movingrois(handles.current_row,handles.current_col)=1; %set the active ROI
    if(max(max(handles.movingrois))~=0) %was the click inside a ROI? but not on corner
        switch buttontype;
            case {'normal','open'} % single, or double click
                %color the active ROI red
                set(handles.recaxis{handles.current_row,handles.current_col},'EdgeColor','r', 'LineStyle', '-');
                set(hObject,'pointer','fleur') %fluer is 4-point arrow
                set(hObject,'WindowButtonMotionFcn',@dragbuttonmotion) %change the function used when mouse moves
                set(hObject,'WindowButtonUpFcn',@dragbuttonup) %change what happens when button released
        end
    else
        set(hObject,'Pointer','arrow')
        set(hObject,'WindowButtonMotionFcn','')
        set(hObject,'WindowButtonUpFcn','')
        handles.newrect=handles.subIrect;
        [handles]=reset_rectangles(hObject, handles); %replot ROIs
    end
end
guidata(hObject,handles);

% --- resize_roimotion occurs if user selected an ROI corner resizing
%     handle
function resize_roimotion(hObject, handles)
handles = guidata(hObject); %load handles
handles.buttonmove=1; %check if button motion happened
%get new cursor location and compare to original location (from roibuttondown)
cp = get(handles.axes1,'CurrentPoint');
xdat = handles.xinit-cp(1,1);
ydat = handles.yinit-cp(1,2);
names=fieldnames(handles); %make a list of all components within the handles structure
srow=handles.activeROI(1);
scol=handles.activeROI(2);
if(handles.activeCorner==1) %resize from top left
    handles.newrect{srow,scol}=[handles.subIrect{srow, scol}(1)-xdat handles.subIrect{srow, scol}(2)-ydat handles.subIrect{srow, scol}(3)+xdat handles.subIrect{srow, scol}(4)+ydat];
    set(hObject,'Pointer','topl');
elseif(handles.activeCorner==2) %resize bottom left
    handles.newrect{srow,scol}=[handles.subIrect{srow, scol}(1)-xdat handles.subIrect{srow, scol}(2) handles.subIrect{srow, scol}(3)+xdat handles.subIrect{srow, scol}(4)-ydat];
    set(hObject,'Pointer','botl');
elseif(handles.activeCorner==3) %resize bottom right
    handles.newrect{srow,scol}=[handles.subIrect{srow, scol}(1) handles.subIrect{srow, scol}(2) handles.subIrect{srow, scol}(3)-xdat handles.subIrect{srow, scol}(4)-ydat];
    set(hObject,'Pointer','botr');
elseif(handles.activeCorner==4) %resize top right
    handles.newrect{srow,scol}=[handles.subIrect{srow, scol}(1) handles.subIrect{srow, scol}(2)-ydat handles.subIrect{srow, scol}(3)-xdat handles.subIrect{srow, scol}(4)+ydat];
    set(hObject,'Pointer','topr');
end
%do not allow ROIs to be resized to zero (ROIS cannot 'flip over a corner')
if(min(handles.newrect{srow,scol}(3))<=0 || min(handles.newrect{srow,scol}(4))<=0)
    handles.newrect{srow,scol}=handles.subIrect{srow,scol};
end
%color resizing ROIs green
[handles]=drawROI(hObject, handles, [srow scol], 'move', 'g', []);
%have corner handles disappear while moving
set(handles.corner(:),'Marker','none');
guidata(hObject,handles);

% --- resize_roiup after button is released during resizing
function resize_roiup(hObject, handles) %load handles
handles = guidata(hObject);
handles.activeCorner=1;
if(handles.buttonmove==1); %button motion happened, use new ROIs
    srow=handles.activeROI(1);
    scol=handles.activeROI(2);
    handles.subIrect{srow,scol}=handles.newrect{srow,scol};
else    
    handles.newrect=handles.subIrect; %no button motion happened, use old ROIs
end
%go back to original drag one ROI function
set(hObject,'WindowButtonMotionFcn','')
set(hObject,'WindowButtonUpFcn','')
%set(hObject,'WindowButtonDownFcn',@roibuttondown)
set(hObject,'WindowButtonDownFcn',@LJBtesting)
guidata(hObject,handles);
%redraw ROI rectangles (includes corner handles)
[handles]=reset_rectangles(hObject, handles); %replot ROIs
set(hObject,'Pointer','arrow')
guidata(hObject,handles);

%-------------------
%set of functions to drag many ROIs, active after select uitoggletoolDrag
%-------------------
function dragbuttondown(hObject, handles)
handles = guidata(hObject);  %load handles
buttontype=get(handles.figure1,'SelectionType');
%get initial cursor location
cp = get(handles.axes1,'CurrentPoint');
handles.xinit = cp(1,1);
handles.yinit = cp(1,2);
%create holder variables
handles.movingrois=false(handles.subrow,handles.subcol);
handles.current_col=[];
handles.current_row=[];
handles.buttonmove=0; %initially set to no motion
handles.newrect=cell(size(handles.subIrect));
%determine if mouse click was inside an ROI
for scol=1:handles.subcol
    for srow=1:handles.subrow
        if(cp(1,1)>=handles.subIrect{srow,scol}(1) && cp(1,1)<=handles.subIrect{srow,scol}(1)+handles.subIrect{srow,scol}(3) && cp(1,2)>=handles.subIrect{srow,scol}(2) && cp(1,2)<=handles.subIrect{srow,scol}(2)+handles.subIrect{srow,scol}(4))
            handles.current_col=scol;
            handles.current_row=srow;
        end
    end
end
%change the dragging function based on the type of mouse click
%regular click drags all ROIs
%right or contol click to drag columns of ROIs
%middle or shift click to drag rows of ROIs
switch buttontype;
case {'normal'} % single left click, drag all
    handles.movingrois(:,:)=1; %set all ROIs as actively moving
    if(~isempty(handles.current_col)) %was the click inside a ROI?
        for scol=1:handles.subcol
            for srow=1:handles.subrow
                %color moving ROIs red
                [handles]=drawROI(hObject, handles, [srow scol], 'change', 'r', []);
            end
        end
        set(hObject,'pointer','fleur') %fluer is 4-point arrow
        set(hObject,'WindowButtonMotionFcn',@dragbuttonmotion) %change the function used when mouse moves
        set(hObject,'WindowButtonUpFcn',@dragbuttonup) %change what happens when button released
    else
      set(hObject,'Pointer','arrow')
      set(hObject,'WindowButtonMotionFcn','')
      set(hObject,'WindowButtonUpFcn','')
      handles.newrect=handles.subIrect;
      [handles]=reset_rectangles(hObject, handles); %replot ROIs
    end
case 'extend' % User click+shift = select row
    handles.movingrois(handles.current_row,:)=1; %set row of ROIs as actively moving
    if(~isempty(handles.current_col)) %was the click inside a ROI?
        for scol=1:handles.subcol
            for srow=handles.current_row
                %color moving ROIs red
                [handles]=drawROI(hObject, handles, [srow scol], 'change', 'r', []);
            end
        end
        set(hObject,'pointer','fleur') %fluer is 4-point arrow
        set(hObject,'WindowButtonMotionFcn',@dragbuttonmotion) %change the function used when mouse moves
        set(hObject,'WindowButtonUpFcn',@dragbuttonup) %change what happens when button released
    else
      set(hObject,'Pointer','arrow')
      set(hObject,'WindowButtonMotionFcn','')
      set(hObject,'WindowButtonUpFcn','')
      handles.newrect=handles.subIrect;
      [handles]=reset_rectangles(hObject, handles); %replot ROIs
    end
case 'alt' % User click+ctrl = select column
    handles.movingrois(:,handles.current_col)=1; %set column of ROIs as actively moving
    if(~isempty(handles.current_col)) %was the click inside a ROI?
        for scol=handles.current_col
            for srow=1:handles.subrow
                %color moving ROIs red
                [handles]=drawROI(hObject, handles, [srow scol], 'change', 'r', []);
            end
        end
        set(hObject,'pointer','fleur') %fluer is 4-point arrow
        set(hObject,'WindowButtonMotionFcn',@dragbuttonmotion) %change the function used when mouse moves
        set(hObject,'WindowButtonUpFcn',@dragbuttonup) %change what happens when button released
    else
      set(hObject,'Pointer','arrow')
      set(hObject,'WindowButtonMotionFcn','')
      set(hObject,'WindowButtonUpFcn','')
      handles.newrect=handles.subIrect;
      [handles]=reset_rectangles(hObject, handles); %replot ROIs
    end
end
guidata(hObject,handles);
 
% --- dragbuttonmotion happens when moving mouse while ROI selected
% --- It is called when dragging one or many ROIs
function dragbuttonmotion(hObject, handles) 
handles = guidata(hObject);  %load handles
handles.buttonmove=1; %check if button motion happened
%get new cursor position and compare to original position
cp = get(handles.axes1,'CurrentPoint');
xdat = handles.xinit-cp(1,1);
ydat = handles.yinit-cp(1,2);
names=fieldnames(handles); %make a list of all components within the handles structure
%change ROI coordinated for actively moving ROIs.  Color moving ROIs red.
for scol=1:handles.subcol
    for srow=1:handles.subrow
        if(handles.movingrois(srow,scol)==1)
            handles.newrect{srow,scol}=[handles.subIrect{srow, scol}(1)-xdat handles.subIrect{srow, scol}(2)-ydat handles.subIrect{srow, scol}(3) handles.subIrect{srow, scol}(4)];
            [handles]=drawROI(hObject, handles, [srow scol], 'move','r',[]);
        end
    end
end
%have corners disappear while moving
set(handles.corner(:),'Marker','none');
guidata(hObject,handles);

% --- dragbuttoup happens after moving mouse while ROI selected
% --- It is called after dragging one or many ROIs
function dragbuttonup(hObject, handles)
handles = guidata(hObject);  %load handles
    if(handles.buttonmove==1); %button motion happened, use new ROIs
        for scol=1:handles.subcol
            for srow=1:handles.subrow
                if(handles.movingrois(srow,scol)==1)
                    %detemine if ROIs were moving and set new coordinates
                    handles.subIrect{srow,scol}=handles.newrect{srow,scol};
                end
            end
        end
    else
        handles.newrect=handles.subIrect; %no button motion happened, use old ROIs
    end
set(hObject,'WindowButtonMotionFcn','') %stop motion function
set(hObject,'WindowButtonUpFcn','')  %stop button_up function
guidata(hObject,handles);
[handles]=reset_rectangles(hObject, handles); %replot ROIs
guidata(hObject,handles);


% --- Clear and Remake display of current ROI rectangles
function [handles]=reset_rectangles(hObject, handles)
names=fieldnames(handles); %make a list of all components within the handles structure
set(handles.figure1,'pointer','arrow')
axes(handles.axes1)
if(handles.parentroi==1);
    %draw the parent ROI
    handles.newparent=handles.parent_axis;
    [handles]=drawROI(hObject, handles, 0, 'change',[],[]);
else
end
for scol=1:handles.subcol
    for srow=1:handles.subrow
        %color ROIs blue
%         [handles]=drawROI(hObject, handles, [srow scol], 'move',[],[]);
        [handles]=drawROI(hObject, handles, [srow scol], 'change',[],[]);
    end
end
%get the active ROI corners
Roi_vert = getRectVertices(handles.subIrect{handles.activeROI(1), handles.activeROI(2)});
%draw resizing corner handles
for cornnum=1:4
    set(handles.corner(cornnum),'Marker','s','MarkerEdgeColor','b', 'Xdata', Roi_vert(cornnum,1),'Ydata',Roi_vert(cornnum,2));
end
handles.activeCorner=0; %no ROI corner is selected
handles.parentresizing=0; %parent is no longer being resized
%Check to make sure parent resizing is not still in effect
%If it is, force back to regular move roi
if(handles.parentresizing==1)
    [handles]=toolsoff(hObject, handles); %deactivate toolbar icons
    handles.parentresizing=0; %parent is no longer being resized
%    set(handles.figure1, 'WindowButtonDownFcn',@roibuttondown); %move ROI function
    set(handles.figure1, 'WindowButtonDownFcn',@LJBtesting); %move ROI function
    set(handles.figure1,'WindowButtonMotionFcn','')
    set(handles.figure1,'WindowButtonUpFcn','')
    set(handles.textHelp, 'String', 'Parent ROI has been resized');
    pasue(0.01);
end
guidata(hObject,handles);


% --- Executes when figure1 is resized.
% --- This function keeps button and text the same size, but shrinks the
%     image, listbox, and histogram as needed.  Minimum sizing exists.
%     Note that initially, figure1_ResizeFcn is active and empty.  This
%     prevents errors on different screen resolutionsfunction figure1_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
function figure1_ResizeFcn(hObject, eventdata, handles)
handles = guidata(hObject); %load handles
Figure_Size = get(hObject, 'Position'); %[hort vert wide high]
Original_Size = handles.figpos;  % Remember the figure's original size in pixel units
if (Figure_Size(3)~=Original_Size(3))  %check width changes
   if(Figure_Size(3) <= 0.75*Original_Size(3))
       newwidth=Original_Size(3)*0.75;  %If the width is too small then reset to 3/4 origianl width.
   else
       newwidth=Figure_Size(3); %Set new width
   end
else
    newwidth=Original_Size(3);
end
if (Figure_Size(4)~=Original_Size(4))  %check height changes
   if(Figure_Size(4) <= 0.8*Original_Size(4))
       newheight=ceil(Original_Size(4)*0.8);  %If the width is too small then reset to 0.8 origianl width.
   else
       newheight=Figure_Size(4); %Set new width
   end
else
    newheight=Original_Size(4);
end
%set new sizes, locations for all panels, axes, and seleceted buttons (as needed)
set(hObject, 'Position',[Figure_Size(1), Figure_Size(2), newwidth, newheight]);
set(handles.axes1, 'Position', [handles.axispos(1) handles.axispos(2) ceil(handles.axisorig(3)-(Original_Size(3)-newwidth)) ceil(handles.axisorig(4)-(Original_Size(4)-newheight))]);
set(handles.axesLogo, 'Position', [handles.logopos(1) newheight-handles.logopos(4) handles.logopos(3) handles.logopos(4)]);
set(handles.axesHist, 'Position', [handles.histpos(1) handles.histpos(2) handles.histpos(3)-(Original_Size(3)-newwidth) handles.histpos(4)]);
set(handles.uipanelCount, 'Position', [handles.Countpanel(1) handles.Countpanel(2)-(Original_Size(4)-newheight) handles.Countpanel(3) handles.Countpanel(4)]);
set(handles.uipanelParam, 'Position', [handles.Parampanel(1) handles.Parampanel(2)-(Original_Size(4)-newheight) handles.Parampanel(3) handles.Parampanel(4)]);
set(handles.uipanelROI, 'Position', [handles.ROIpanel(1) handles.ROIpanel(2)-(Original_Size(4)-newheight) handles.ROIpanel(3) handles.ROIpanel(4)]);
set(handles.uipanelStored, 'Position', [handles.Storedpanel(1) handles.Storedpanel(2) handles.Storedpanel(3) handles.Storedpanel(4)-(Original_Size(4)-newheight)]);
set(handles.listboxData, 'Position', [handles.listboxpos(1) handles.listboxpos(2) handles.listboxpos(3) handles.listboxpos(4)-(Original_Size(4)-newheight)]);
set(handles.uipanelTitle, 'Position', [handles.Titlepanel(1) newheight-handles.Titlepanel(4) handles.Titlepanel(3)*newwidth/Original_Size(3) handles.Titlepanel(4)]);
set(handles.textHelp, 'Position', [handles.helppos(1) handles.helppos(2) handles.helppos(3)*newwidth/Original_Size(3) handles.helppos(4)]);
set(handles.pushbuttonTabResults, 'Position',[handles.tabRpos(1) handles.tabRpos(2)-(Original_Size(4)-newheight) handles.tabRpos(3) handles.tabRpos(4)]);
set(handles.pushbuttonTabImages, 'Position',[handles.tabIpos(1) handles.tabIpos(2)-(Original_Size(4)-newheight) handles.tabIpos(3) handles.tabIpos(4)]);
handles.axispos=get(handles.axes1, 'Position'); %store new axis1 Position
[handles]=newaxis(hObject,handles);
%determine the computer's screen size, inorder to maximize image appropriately
screensize=get(0,'ScreenSize'); 
newfigpos=get(hObject,'Position');
%If the GUI is offscreen, move on screen
if(min(newfigpos)<1 || (newfigpos(2)+newfigpos(4)>screensize(4)))
    movegui(hObject, 'onscreen')
end
guidata(hObject,handles);


%--- Move and Resize Parent Functions
%    Set of functions to move Parent ROI
%-------------------
function parentroi_buttondown(hObject, handles)
handles = guidata(hObject); %load handles
buttontype=get(handles.figure1,'SelectionType');
%capture current cursor location
cp = get(handles.axes1,'CurrentPoint');
handles.xinit = cp(1,1);
handles.yinit = cp(1,2);
grabbed_edge=false; %grabbed_edge refers to resizing handles
%check the resizing handles to see if they have been selected
for cornnum=1:4
    if(strcmp(get(handles.parent_corner(cornnum),'UserData'),'on'))
        grabbed_edge=true;
        handles.activeCorner=cornnum; %get the active corner
        set(gcf,'WindowButtonMotionFcn',@parentroi_resize_motion) %change the function used when mouse moves
        set(gcf,'WindowButtonUpFcn',@parentroi_resize_up) %change what happens when button released
    end
end
if(~grabbed_edge); %no ROI corner is selected
    handles.newparent=[];
    handles.buttonmove=0; %check if button motion happened
    handles.activeparent=false;
    %determine if button press occurred inside an ROI
    if(cp(1,1)>=handles.parentrect(1) && cp(1,1)<=handles.parentrect(1)+handles.parentrect(3) && cp(1,2)>=handles.parentrect(2) && cp(1,2)<=handles.parentrect(2)+handles.parentrect(4))
        handles.activeparent=true; %click was inside parent
    end
    if(handles.activeparent) %was the click inside the parent? but not on corner
        switch buttontype;
            case {'normal'} % single click
                %moving the parent ROI
                set(handles.figure1,'pointer','fleur') %fluer is 4-point arrow
                set(handles.figure1,'WindowButtonMotionFcn',@parentroi_motion) %change the function used when mouse moves
                set(handles.figure1,'WindowButtonUpFcn',@parentroi_buttonup) %change what happens when button released
            case {'open'} % double click
                set(hObject,'Pointer','arrow')
%                set(handles.figure1, 'WindowButtonDownFcn',@roibuttondown); %move ROI function
                set(handles.figure1, 'WindowButtonDownFcn',@LJBtesting); %move ROI function
                set(handles.figure1,'WindowButtonMotionFcn','')
                set(handles.figure1,'WindowButtonUpFcn','')
                [handles]=fastroi(hObject, handles); %recalculate ROIs
                [handles]=plotsubIrect(hObject, handles); %redraw image
                [handles]=plothist(hObject, handles, 'on'); %recalculate histogram
                set(handles.textHelp, 'String', 'Parent ROI has been resized');
                %now force to single-click mode to allow repeated selections of Parent ROI
                %(otherwise next selection may immediately deactivate)
                set(gcf,'SelectionType','normal')
                guidata(hObject,handles)
        end
    else
        set(hObject,'Pointer','arrow')
    end
end
guidata(hObject,handles);

% --- parentroi_resize_motion occurs if user selected an ROI corner resizing
%     handle
function parentroi_resize_motion(hObject, handles)
handles = guidata(hObject); %load handles
handles.buttonmove=1; %check if button motion happened
%get new cursor location and compare to original location (from roibuttondown)
cp = get(handles.axes1,'CurrentPoint');
xdat = handles.xinit-cp(1,1);
ydat = handles.yinit-cp(1,2);
names=fieldnames(handles); %make a list of all components within the handles structure
srow=handles.activeROI(1);
scol=handles.activeROI(2);
if(handles.activeCorner==1) %resize from top left
    handles.newparent=[handles.parentrect(1)-xdat handles.parentrect(2)-ydat handles.parentrect(3)+xdat handles.parentrect(4)+ydat];
    set(hObject,'Pointer','topl');
elseif(handles.activeCorner==2) %resize bottom left
    handles.newparent=[handles.parentrect(1)-xdat handles.parentrect(2) handles.parentrect(3)+xdat handles.parentrect(4)-ydat];
    set(hObject,'Pointer','botl');
elseif(handles.activeCorner==3) %resize bottom right
    handles.newparent=[handles.parentrect(1) handles.parentrect(2) handles.parentrect(3)-xdat handles.parentrect(4)-ydat];
    set(hObject,'Pointer','botr');
elseif(handles.activeCorner==4) %resize top right
    handles.newparent=[handles.parentrect(1) handles.parentrect(2)-ydat handles.parentrect(3)-xdat handles.parentrect(4)+ydat];
    set(hObject,'Pointer','topr');
end
%do not allow ROIs to be resized to zero (ROIS cannot 'flip over a corner')
if(min(handles.newparent(3))<=0 || min(handles.newparent(4))<=0)
    handles.newparent=handles.parentrect;
end
[handles]=drawROI(hObject, handles, 0, 'move', [], '--');
%have corner handles disappear while moving
set(handles.parent_corner(:),'Marker','none');
guidata(hObject,handles);

% --- parentroi_resize_up after button is released during resizing
function parentroi_resize_up(hObject, handles) %load handles
handles = guidata(hObject);
handles.activeCorner=1;
if(handles.buttonmove==1); %button motion happened, use new ROIs
    srow=handles.activeROI(1);
    scol=handles.activeROI(2);
    handles.parentrect=handles.newparent;
else    
    handles.newrect=handles.subIrect; %no button motion happened, use old ROIs
end
%go back to original drag one ROI function
set(handles.figure1,'WindowButtonMotionFcn','')
set(handles.figure1,'WindowButtonUpFcn','')
set(handles.figure1,'WindowButtonDownFcn',@parentroi_buttondown)
guidata(hObject,handles);
%draw parent resizing corner handles
Roi_vert = getRectVertices(handles.parentrect);
for cornnum=1:4
    set(handles.parent_corner(cornnum),'Marker','s','MarkerEdgeColor','m', 'SizeData', 120, 'Xdata', Roi_vert(cornnum,1),'Ydata',Roi_vert(cornnum,2));
end
handles.activeCorner=0; %no ROI corner is selected
set(hObject,'Pointer','arrow')
guidata(hObject,handles);

% --- parentroi_motion happens when moving mouse while ROI selected
% --- It is called when dragging one or many ROIs
function parentroi_motion(hObject, handles) 
handles = guidata(hObject);  %load handles
handles.buttonmove=1; %check if button motion happened
%get new cursor position and compare to original position
cp = get(handles.axes1,'CurrentPoint');
xdat = handles.xinit-cp(1,1);
ydat = handles.yinit-cp(1,2);
names=fieldnames(handles); %make a list of all components within the handles structure
%change parent ROI coordinated for actively moving ROIs.  Parent always magenta.
handles.newparent=[handles.parentrect(1)-xdat handles.parentrect(2)-ydat handles.parentrect(3) handles.parentrect(4)];
set(handles.parent_axis,'Position',handles.newparent,'EdgeColor','m', 'LineStyle', '--');
%have corners disappear while moving
set(handles.parent_corner(:),'Marker','none');
guidata(hObject,handles);

% --- parentroi_buttonup happens after moving mouse while ROI selected
% --- It is called after dragging one or many ROIs
function parentroi_buttonup(hObject, handles)
handles = guidata(hObject);  %load handles
if(handles.buttonmove==1); %button motion happened, use new ROIs
    %detemine if ROIs were moving and set new coordinates
    handles.parentrect=handles.newparent;
else
    handles.newparent=handles.parentrect; %no button motion happened, use old ROIs
end
set(hObject,'WindowButtonMotionFcn','') %stop motion function
set(hObject,'WindowButtonUpFcn','')  %stop button_up function
guidata(hObject,handles);
Roi_vert = getRectVertices(handles.parentrect);
for cornnum=1:4
    set(handles.parent_corner(cornnum),'Marker','s','MarkerEdgeColor','m', 'SizeData', 120, 'Xdata', Roi_vert(cornnum,1),'Ydata',Roi_vert(cornnum,2));
end
set(handles.figure1,'pointer','arrow')
guidata(hObject,handles);


% --------------------------------------------------------------------
function ROI_Shapes_Callback(hObject, eventdata, handles)
% hObject    handle to ROI_Shapes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function ROI_Rectangle_Callback(hObject, eventdata, handles)
% hObject    handle to ROI_Rectangle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(strcmp(get(handles.ROI_Rectangle, 'Checked'),'on'))
else
    set(handles.ROI_Rectangle, 'Checked','on')
    set(handles.ROI_Elliptical, 'Checked','off')
    handles.ROI_shape='rectangle';
    [handles]=plotsubIrect(hObject, handles); %plot image with ROIs
end

% --------------------------------------------------------------------
function ROI_Elliptical_Callback(hObject, eventdata, handles)
% hObject    handle to ROI_Elliptical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(strcmp(get(handles.ROI_Elliptical, 'Checked'),'on'))
else
    set(handles.ROI_Rectangle, 'Checked','off')
    set(handles.ROI_Elliptical, 'Checked','on')
    handles.ROI_shape='ellipse';
    [handles]=plotsubIrect(hObject, handles); %plot image with ROIs
end


% --------------------------------------------------------------------
function Parent_ROI_Shape_Callback(hObject, eventdata, handles)
% hObject    handle to Parent_ROI_Shape (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Indiv_ROI_Shape_Callback(hObject, eventdata, handles)
% hObject    handle to Indiv_ROI_Shape (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Shape_Indiv_Rect_Callback(hObject, eventdata, handles)
% hObject    handle to Shape_Indiv_Rect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(strcmp(get(handles.Shape_Indiv_Rect, 'Checked'),'on'))
else
    set(handles.Shape_Indiv_Rect, 'Checked','on')
    set(handles.Shape_Indiv_Ellip, 'Checked','off')
    handles.ROI_shape='rectangle';
    [handles]=fastroi(hObject, handles); %determine ROI position
    [handles]=plotsubIrect(hObject, handles); %plot image with ROIs
end

% --------------------------------------------------------------------
function Shape_Indiv_Ellip_Callback(hObject, eventdata, handles)
% hObject    handle to Shape_Indiv_Ellip (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(strcmp(get(handles.Shape_Indiv_Ellip, 'Checked'),'on'))
else
    set(handles.Shape_Indiv_Rect, 'Checked','off')
    set(handles.Shape_Indiv_Ellip, 'Checked','on')
    handles.ROI_shape='ellipse';
    [handles]=fastroi(hObject, handles); %determine ROI position
    [handles]=plotsubIrect(hObject, handles); %plot image with ROIs
end

% --------------------------------------------------------------------
function Shape_Parent_Rect_Callback(hObject, eventdata, handles)
% hObject    handle to Shape_Parent_Rect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(strcmp(get(handles.Shape_Parent_Rect, 'Checked'),'on'))
else
    set(handles.Shape_Parent_Rect, 'Checked','on')
    set(handles.Shape_Parent_Ellip, 'Checked','off')
    handles.ROI_parentshape='rectangle';
    [handles]=fastroi(hObject, handles); %determine ROI position
    [handles]=plotsubIrect(hObject, handles); %plot image with ROIs
    [handles]=plothist(hObject, handles, 'on'); %recalculate histogram
end

% --------------------------------------------------------------------
function Shape_Parent_Ellip_Callback(hObject, eventdata, handles)
% hObject    handle to Shape_Parent_Ellip (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(strcmp(get(handles.Shape_Parent_Ellip, 'Checked'),'on'))
else
    set(handles.Shape_Parent_Rect, 'Checked','off')
    set(handles.Shape_Parent_Ellip, 'Checked','on')
    handles.ROI_parentshape='ellipse';
    [handles]=fastroi(hObject, handles); %determine ROI position
    [handles]=plotsubIrect(hObject, handles); %plot image with ROIs
    [handles]=plothist(hObject, handles, 'on'); %recalculate histogram
end

% --- drawROI writes the individual or parent ROI, checking the ROI shape.
% --- It is called by many functions.
% --- ROIid tells which individual ROI [srow scol] or parent (0) is being
%     drawn.
% --- The redraw parameter determines whether the ROI are completely
%     redrawn or the properties of existing ROIs are changed.
function [handles]=drawROI(hObject, handles, ROIid, redraw, ROIcolor, ROIline)
if(isempty(ROIline))
    ROIline='-';
end
if(ROIid==0) %Parent ROI
    if(isempty(ROIcolor))
        ROIcolor='m';
    end
    switch redraw;
        case {'new'} % ROIs are being redrawn completely
%            ROIcolor='b';
            if(strcmp(handles.ROI_parentshape, 'rectangle'))
                handles.parent_axis=rectangle('Position',handles.parentrect,'EdgeColor',ROIcolor, 'LineWidth', 3, 'LineStyle', ROIline);
            elseif(strcmp(handles.ROI_parentshape, 'ellipse'))
                handles.parent_axis=rectangle('Position',handles.parentrect,'EdgeColor',ROIcolor, 'LineWidth', 3, 'LineStyle', ROIline,'Curvature',[1,1]);
            else
                errordlg('Unable to determine Parent ROI shape selection.','ROI error', 'modal')
            end
        case {'change'} % ROIs have not moved, but properties have changed
%            ROIcolor='m';
            if(strcmp(handles.ROI_parentshape, 'rectangle'))
                set(handles.parent_axis,'Position',handles.parentrect,'EdgeColor',ROIcolor, 'LineWidth', 3, 'LineStyle', ROIline);
            elseif(strcmp(handles.ROI_parentshape, 'ellipse'))
                set(handles.parent_axis,'Position',handles.parentrect,'EdgeColor',ROIcolor, 'LineWidth', 3, 'LineStyle', ROIline,'Curvature',[1,1]);
            else
                errordlg('Unable to determine Parent ROI shape selection.','ROI error', 'modal')
            end
        case {'move'} % ROIs have moved, change existing properties
 %           ROIcolor='m';
            if(strcmp(handles.ROI_parentshape, 'rectangle'))
                set(handles.parent_axis,'Position',handles.newparent,'EdgeColor',ROIcolor, 'LineWidth', 3, 'LineStyle', ROIline);
            elseif(strcmp(handles.ROI_parentshape, 'ellipse'))
                set(handles.parent_axis,'Position',handles.newparent,'EdgeColor',ROIcolor, 'LineWidth', 3, 'LineStyle', ROIline,'Curvature',[1,1]);
            else
                errordlg('Unable to determine Parent ROI shape selection.','ROI error', 'modal')
            end
        otherwise
            errordlg('Unknown ROI redraw command.','ROI error', 'modal')
    end
else %indiv ROIs
    if(isempty(ROIcolor))
        ROIcolor='b';
    end
    switch redraw;
        case {'new'} % ROIs are being redrawn completely
            if(strcmp(handles.ROI_shape, 'rectangle'))
                handles.recaxis{ROIid(1),ROIid(2)}=rectangle('Position',handles.subIrect{ROIid(1),ROIid(2)},'EdgeColor',ROIcolor);
            elseif(strcmp(handles.ROI_shape, 'ellipse'))
                handles.recaxis{ROIid(1),ROIid(2)}=rectangle('Position',handles.subIrect{ROIid(1),ROIid(2)},'EdgeColor',ROIcolor,'Curvature',[1,1]);
            else
                errordlg('Unable to determine ROI shape selection.','ROI error', 'modal')
            end
        case {'change'} % ROIs have not moved, but properties have changed
            if(~isempty(handles.newrect{ROIid(1),ROIid(2)})) %did this ROI move?
                if(strcmp(handles.ROI_shape, 'rectangle'))
                    set(handles.recaxis{ROIid(1),ROIid(2)},'Position',handles.newrect{ROIid(1),ROIid(2)},'EdgeColor',ROIcolor);
                elseif(strcmp(handles.ROI_shape, 'ellipse'))
                    set(handles.recaxis{ROIid(1),ROIid(2)},'Position',handles.newrect{ROIid(1),ROIid(2)},'EdgeColor',ROIcolor,'Curvature',[1,1]);
                else
                    errordlg('Unable to determine ROI shape selection.','ROI error', 'modal')
                end
            end
        case {'move'} % ROIs have moved, change existing properties
            if(~isempty(handles.newrect{ROIid(1),ROIid(2)})) %did this ROI move?
                if(strcmp(handles.ROI_shape, 'rectangle'))
                    set(handles.recaxis{ROIid(1),ROIid(2)},'Position',handles.newrect{ROIid(1),ROIid(2)},'EdgeColor',ROIcolor);
                elseif(strcmp(handles.ROI_shape, 'ellipse'))
                    set(handles.recaxis{ROIid(1),ROIid(2)},'Position',handles.newrect{ROIid(1),ROIid(2)},'EdgeColor',ROIcolor,'Curvature',[1,1]);
                else
                    errordlg('Unable to determine ROI shape selection.','ROI error', 'modal')
                end
            end
        otherwise
            errordlg('Unknown ROI redraw command.','ROI error', 'modal')
    end
end

%
%   This function replaces roibuttondown LJB Feb 2017
%
function LJBtesting(hObject, handles)
handles = guidata(hObject);  %load handles
buttontype=get(handles.figure1,'SelectionType');
%get initial cursor location
cp = get(handles.axes1,'CurrentPoint');
handles.xinit = cp(1,1);
handles.yinit = cp(1,2);
%create holder variables
handles.movingrois=false(handles.subrow,handles.subcol);
handles.current_col=[];
handles.current_row=[];
handles.buttonmove=0; %initially set to no motion
handles.newrect=cell(size(handles.subIrect));
%determine if mouse click was inside an ROI
for scol=1:handles.subcol
    for srow=1:handles.subrow
        if(cp(1,1)>=handles.subIrect{srow,scol}(1) && cp(1,1)<=handles.subIrect{srow,scol}(1)+handles.subIrect{srow,scol}(3) && cp(1,2)>=handles.subIrect{srow,scol}(2) && cp(1,2)<=handles.subIrect{srow,scol}(2)+handles.subIrect{srow,scol}(4))
            handles.current_col=scol;
            handles.current_row=srow;
        end
    end
end
%change the dragging function based on the type of mouse click
%regular click drags all ROIs
%right or contol click to drag columns of ROIs
%middle or shift click to drag rows of ROIs
switch buttontype;
case {'normal'} % single left click, operate on a single roi
grabbed_edge=false; %grabbed_edge refers to resizing handles
%check the resizing handles to see if they have been selected
for cornnum=1:4
    if(strcmp(get(handles.corner(cornnum),'UserData'),'on'))
        grabbed_edge=true;
        handles.activeCorner=cornnum; %get the active corner
        set(gcf,'WindowButtonMotionFcn',@resize_roimotion) %change the function used when mouse moves
        set(gcf,'WindowButtonUpFcn',@resize_roiup) %change what happens when button released
    end
end
if(~grabbed_edge); %no ROI corner is selected
    %make holder variables
    handles.current_col=[];
    handles.current_row=[];
    handles.newrect=cell(size(handles.subIrect));
    handles.movingrois=false(handles.subrow,handles.subcol);
    handles.buttonmove=0; %check if button motion happened
    %determine if button press occurred inside an ROI
    for scol=1:handles.subcol
        for srow=1:handles.subrow
            if(cp(1,1)>=handles.subIrect{srow,scol}(1) && cp(1,1)<=handles.subIrect{srow,scol}(1)+handles.subIrect{srow,scol}(3) && cp(1,2)>=handles.subIrect{srow,scol}(2) && cp(1,2)<=handles.subIrect{srow,scol}(2)+handles.subIrect{srow,scol}(4))
                handles.current_col=scol;
                handles.current_row=srow;
                handles.activeROI=[srow scol]; %get the active ROI
            end
        end
    end
    handles.movingrois(handles.current_row,handles.current_col)=1; %set the active ROI
    if(max(max(handles.movingrois))~=0) %was the click inside a ROI? but not on corner
        switch buttontype;
            case {'normal','open'} % single, or double click
                %color the active ROI red
                set(handles.recaxis{handles.current_row,handles.current_col},'EdgeColor','r', 'LineStyle', '-');
                set(hObject,'pointer','fleur') %fluer is 4-point arrow
                set(hObject,'WindowButtonMotionFcn',@dragbuttonmotion) %change the function used when mouse moves
                set(hObject,'WindowButtonUpFcn',@dragbuttonup) %change what happens when button released
        end
    else
        set(hObject,'Pointer','arrow')
        set(hObject,'WindowButtonMotionFcn','')
        set(hObject,'WindowButtonUpFcn','')
        handles.newrect=handles.subIrect;
        [handles]=reset_rectangles(hObject, handles); %replot ROIs
    end
end
case 'extend' % User click+shift = select row
    handles.movingrois(handles.current_row,:)=1; %set row of ROIs as actively moving
    if(~isempty(handles.current_col)) %was the click inside a ROI?
        for scol=1:handles.subcol
            for srow=handles.current_row
                %color moving ROIs red
                [handles]=drawROI(hObject, handles, [srow scol], 'change', 'r', []);
            end
        end
        set(hObject,'pointer','fleur') %fluer is 4-point arrow
        set(hObject,'WindowButtonMotionFcn',@dragbuttonmotion) %change the function used when mouse moves
        set(hObject,'WindowButtonUpFcn',@dragbuttonup) %change what happens when button released
    else
      set(hObject,'Pointer','arrow')
      set(hObject,'WindowButtonMotionFcn','')
      set(hObject,'WindowButtonUpFcn','')
      handles.newrect=handles.subIrect;
      [handles]=reset_rectangles(hObject, handles); %replot ROIs
    end
case 'alt' % User click+ctrl = select column
    handles.movingrois(:,handles.current_col)=1; %set column of ROIs as actively moving
    if(~isempty(handles.current_col)) %was the click inside a ROI?
        for scol=handles.current_col
            for srow=1:handles.subrow
                %color moving ROIs red
                [handles]=drawROI(hObject, handles, [srow scol], 'change', 'r', []);
            end
        end
        set(hObject,'pointer','fleur') %fluer is 4-point arrow
        set(hObject,'WindowButtonMotionFcn',@dragbuttonmotion) %change the function used when mouse moves
        set(hObject,'WindowButtonUpFcn',@dragbuttonup) %change what happens when button released
    else
      set(hObject,'Pointer','arrow')
      set(hObject,'WindowButtonMotionFcn','')
      set(hObject,'WindowButtonUpFcn','')
      handles.newrect=handles.subIrect;
      [handles]=reset_rectangles(hObject, handles); %replot ROIs
    end
end
guidata(hObject,handles);       

