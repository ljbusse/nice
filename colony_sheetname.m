function varargout = colony_sheetname(varargin)
% colony_sheetname is called from NICE_GUI_xxx
%       It asks for a sheetname when exporting to Excel.
%
%       Example:
%       sheetout = colony_sheetname([],[],[],PathName, FileName);
%
%     NICE was developed by:
%     Matthew Clarke* (matthew.clarke@nist.gov) and 
%     Jeeseong Hwang (jeeseong.hwang@nist.gov)
%     Optical Technology Division
%     National Institute of Standards and Technology
%     Gaithersburg, MD 20899
% 
%     In collaboration with:
%     Robert Burton and Moon Nahm, University of Alabama � Birmingham
% 
%     Citation:
%     Matthew L. Clarke, Robert L. Burton, A. Nayo Hill, Maritoni Litorja, 
%     Moon H. Nahm,Jeeseong Hwang, 
%     "Low-Cost, High-Throughput, Automated Counting of Bacterial Colonies" 
%     Cytometry A, 77A, 790-797 (2010)
%
%     *Program code was developed by and correspondence should be addressed
%     to Matthew Clarke.  Correspondence may be sent to nice@nist.gov.
% 
%     Support for this project was provided in part by PATH.  The views 
%     expressed by the authors do not necessarily reflect the views of 
%     PATH.
%
%     Created and modified, 2008-2009
%
%  Legal Disclaimer:
%  NIST�s Integrated Colony Enumerator (NICE) software was developed at the
%  National Institute of Standards and Technology by employees of the
%  Federal Government in the course of their official duties. Pursuant to
%  Title 17 Section 105 of the United States Code this software is not
%  subject to copyright protection and is in the public domain. Support for
%  this project was provided in part by PATH.  The views expressed by the
%  developers of the software do not necessarily reflect the views of PATH.
%  NICE is an experimental system. Neither NIST nor PATH assumes any
%  responsibility whatsoever for its use by other parties, and makes no
%  guarantees, expressed or implied, about its quality, reliability, or any
%  other characteristic. We would appreciate acknowledgment if the software
%  is used. This software can be redistributed and/or modified freely
%  provided that any derivative works bear some notice that they are
%  derived from it, and any modified versions bear some notice that they
%  have been modified. 
%
% COLONY_SHEETNAME M-file for colony_sheetname.fig
%      COLONY_SHEETNAME, by itself, creates a new COLONY_SHEETNAME or raises the existing
%      singleton*.
%
%      H = COLONY_SHEETNAME returns the handle to a new COLONY_SHEETNAME or the handle to
%      the existing singleton*.
%
%      COLONY_SHEETNAME('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COLONY_SHEETNAME.M with the given input arguments.
%
%      COLONY_SHEETNAME('Property','Value',...) creates a new COLONY_SHEETNAME or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before colony_sheetname_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to colony_sheetname_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help colony_sheetname

% Last Modified by GUIDE v2.5 24-Sep-2008 10:52:21

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @colony_sheetname_OpeningFcn, ...
                   'gui_OutputFcn',  @colony_sheetname_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before colony_sheetname is made visible.
function colony_sheetname_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to colony_sheetname (see VARARGIN)
handles.sheetname=('Sheet1'); %default name is Sheet1
set(handles.editSheet, 'String', handles.sheetname);
handles.goahead=0; %if user cancels out, do not save
%set "enter/return" to operate just like clikcing "OK"
set([handles.figure1],'KeyPressFcn',@keypress_execute);
%Create structure for output
handles.sout=cell(2,1);
handles.sout{1}=handles.sheetname;
handles.sout{2}=handles.goahead;
pathstr=varargin{4}; %get the save path
cd(pathstr);
handles.filename=varargin{5}; %get the Excel filename
set(handles.textFilename, 'String', handles.filename);

% Choose default command line output for colony_sheetname
handles.output = handles.sout;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes colony_sheetname wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = colony_sheetname_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
% The figure can be deleted now
delete(handles.figure1);


function editSheet_Callback(hObject, eventdata, handles)
% hObject    handle to editSheet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of editSheet as text
%        str2double(get(hObject,'String')) returns contents of editSheet as a double
sheetname=strtrim(get(hObject, 'String'));
if(length(sheetname)>31)
    sheetname=sheetname(1:31);
    set(handles.sheetname, 'String', sheetname);
    errordlg('Sheet name too long - shortened to 31 characters','Naming Error', 'modal')
end
handles.sheetname=sheetname;
guidata(hObject,handles)  % Save the new value

% --- Executes during object creation, after setting all properties.
function editSheet_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editSheet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbuttonOK.
function pushbuttonOK_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(exist(handles.filename, 'file')~=0) %determin if file exists
    [typ, desc]=xlsfinfo(handles.filename); %get info about Excel file
    needover=max(strcmp(handles.sheetname,desc(:))); %check if Sheet already exists
    if(needover==1) %if so, ask to overwrite
        handles.question=('Overwrite existing sheet?');
        handles.confirm = confirm_gui([],[],[],handles.question); %3 blank, then question
        if(handles.confirm==1); %overwrite
            handles.goahead=1;
            handles.sout{1}=handles.sheetname;
            handles.sout{2}=handles.goahead;
            handles.output = handles.sout;
            guidata(hObject, handles);
            uiresume(handles.figure1);
        else
            handles.goahead=0; 
            guidata(hObject, handles);
        end
    else
        handles.goahead=1;
        handles.sout{1}=handles.sheetname;
        handles.sout{2}=handles.goahead;
        handles.output = handles.sout;
        guidata(hObject, handles);
        uiresume(handles.figure1);
    end
else
    handles.goahead=1;
    handles.sout{1}=handles.sheetname;
    handles.sout{2}=handles.goahead;
    handles.output = handles.sout;
    guidata(hObject, handles);
    uiresume(handles.figure1);
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: delete(hObject) closes the figure
handles.goahead=0;
handles.sout{1}=handles.sheetname;
handles.sout{2}=handles.goahead;
handles.output = handles.sout;
guidata(hObject, handles);
uiresume(handles.figure1);

% --- Monitors if user hits "enter"
%keypress_execute takes key button and uses to execute callback
%eventdata collects the keystroke
function keypress_execute(hObject,eventdata)
%brings in the handles structure in to the function
handles = guidata(hObject); %load handles
key= eventdata.Key;
if(strcmp(key,'return')) %user pressed 'return/enter'
    %set "OK" button as new source
    hObject = handles.pushbuttonOK; 
    pushbuttonOK_Callback(hObject, [], handles);
end
