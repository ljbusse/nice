function varargout = confirm_gui(varargin)
% confirm_gui is called from different GUI applications
%       It displays a yes/no dialog with an input question.
%
%       Example:
%       delcon = confirm_gui([],[],[],handles.question);
%
%           Matthew Clarke
%           Optical Technology Division
%           National Institute of Standards and Technology
%
%       Created and modified, 2008-2009
%
% CONFIRM_GUI M-file for confirm_gui.fig
%      CONFIRM_GUI, by itself, creates a new CONFIRM_GUI or raises the existing
%      singleton*.
%
%      H = CONFIRM_GUI returns the handle to a new CONFIRM_GUI or the handle to
%      the existing singleton*.
%
%      CONFIRM_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CONFIRM_GUI.M with the given input arguments.
%
%      CONFIRM_GUI('Property','Value',...) creates a new CONFIRM_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before confirm_gui_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to confirm_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help confirm_gui

% Last Modified by GUIDE v2.5 14-May-2008 13:55:25

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @confirm_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @confirm_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before confirm_gui is made visible.
function confirm_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to confirm_gui (see VARARGIN)
handles.question=varargin{4}; %load input question
handles.answer=0; %default answer is no
set(handles.textQuestion, 'String', handles.question);

% Choose default command line output for confirm_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes confirm_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);
uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = confirm_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
% The figure can be deleted now
delete(handles.figure1);

% --- Executes on button press in pushbuttonYes.
function pushbuttonYes_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonYes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.answer=1; %switch answer to yes
handles.output = handles.answer;
guidata(hObject, handles);
uiresume(handles.figure1);

% --- Executes on button press in pushbuttonNo.
function pushbuttonNo_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonNo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.answer=0; %answer is no
handles.output = handles.answer;
guidata(hObject, handles);
uiresume(handles.figure1);



% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isequal(get(handles.figure1, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(handles.figure1);
else
    % The GUI is no longer waiting, just close it
    delete(handles.figure1);
end