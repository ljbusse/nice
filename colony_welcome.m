function varargout = colony_welcome(varargin)
% colony_welcome is called from NICE_GUI_xxx
%       It displays a login screen for the user to enter details.
%
%       Examples:
%       handles.expdetails = colony_welcome;
%       handles.expdetails = colony_welcome(handles.expdetails);
%
%     NICE was developed by:
%     Matthew Clarke* (matthew.clarke@nist.gov) and 
%     Jeeseong Hwang (jeeseong.hwang@nist.gov)
%     Optical Technology Division
%     National Institute of Standards and Technology
%     Gaithersburg, MD 20899
% 
%     In collaboration with:
%     Robert Burton and Moon Nahm, University of Alabama � Birmingham
% 
%     Citation:
%     Matthew L. Clarke, Robert L. Burton, A. Nayo Hill, Maritoni Litorja, 
%     Moon H. Nahm,Jeeseong Hwang, 
%     "Low-Cost, High-Throughput, Automated Counting of Bacterial Colonies" 
%     Cytometry A, 77A, 790-797 (2010)
%
%     *Program code was developed by and correspondence should be addressed
%     to Matthew Clarke.  Correspondence may be sent to nice@nist.gov.
% 
%     Support for this project was provided in part by PATH.  The views 
%     expressed by the authors do not necessarily reflect the views of 
%     PATH.
%
%     Created and modified, 2008-2009
%
%  Legal Disclaimer:
%  NIST�s Integrated Colony Enumerator (NICE) software was developed at the
%  National Institute of Standards and Technology by employees of the
%  Federal Government in the course of their official duties. Pursuant to
%  Title 17 Section 105 of the United States Code this software is not
%  subject to copyright protection and is in the public domain. Support for
%  this project was provided in part by PATH.  The views expressed by the
%  developers of the software do not necessarily reflect the views of PATH.
%  NICE is an experimental system. Neither NIST nor PATH assumes any
%  responsibility whatsoever for its use by other parties, and makes no
%  guarantees, expressed or implied, about its quality, reliability, or any
%  other characteristic. We would appreciate acknowledgment if the software
%  is used. This software can be redistributed and/or modified freely
%  provided that any derivative works bear some notice that they are
%  derived from it, and any modified versions bear some notice that they
%  have been modified. 
%
% COLONY_WELCOME M-file for colony_welcome.fig
%      COLONY_WELCOME, by itself, creates a new COLONY_WELCOME or raises the existing
%      singleton*.
%
%      H = COLONY_WELCOME returns the handle to a new COLONY_WELCOME or the handle to
%      the existing singleton*.
%
%      COLONY_WELCOME('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COLONY_WELCOME.M with the given input arguments.
%
%      COLONY_WELCOME('Property','Value',...) creates a new COLONY_WELCOME or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before colony_welcome_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to colony_welcome_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help colony_welcome

% Last Modified by GUIDE v2.5 29-Jun-2009 08:09:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @colony_welcome_OpeningFcn, ...
                   'gui_OutputFcn',  @colony_welcome_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before colony_welcome is made visible.
function colony_welcome_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to colony_welcome (see VARARGIN)
Ilogo=imread('NICElogo bkgd.jpg'); %load logo
axes(handles.axesLogo);
imshow(Ilogo);
axis off
%set "enter/return" to operate just like clikcing "Done"
set(handles.figure1,'KeyPressFcn',@keypress_execute); 
set([handles.editDate, handles.editExp, handles.editName, handles.editNotes], 'KeyPressFcn', @keypress_execute);
if(nargin<4)
    %If called during NICE opening function, set default answers (in case none
    %  entered)
    handles.exptime=datestr(fix(clock)); %get time
    set(handles.editDate, 'String', handles.exptime); 
    handles.expname=('none'); %operator name
    handles.expexp=('none'); %experiment name
    handles.expnotes=('none'); %notes
    handles.expinfo=cell(4,1); %Date, Operator, Experiment, Notes
    handles.expinfo{1}=handles.exptime;
    handles.expinfo{2}=handles.expname;
    handles.expinfo{3}=handles.expexp;
    handles.expinfo{4}=handles.expnotes;
else
    %If called later from NICE, the previous entries as default
    %Display these entries
    handles.expinfo=cell(4,1); %Date, Operator, Experiment, Notes
    expinfo=varargin;
    handles.exptime=expinfo{1,1}{1};
    handles.expname=expinfo{1,1}{2};
    handles.expexp=expinfo{1,1}{3};
    handles.expnotes=expinfo{1,1}{4};
    handles.expinfo{1}=handles.exptime;
    handles.expinfo{2}=handles.expname;
    handles.expinfo{3}=handles.expexp;
    handles.expinfo{4}=handles.expnotes;
    set(handles.editDate, 'String', handles.exptime);
    set(handles.editExp, 'String', handles.expexp);
    set(handles.editName, 'String', handles.expname);
    set(handles.editNotes, 'String', handles.expnotes);
    handles.output = handles.expinfo;
end
    guidata(hObject, handles);
    uiresume(handles.figure1);

% Choose default command line output for colony_welcome
handles.output = handles.expinfo;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes colony_welcome wait for user response (see UIRESUME)
% uiwait(handles.figure1);
uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = colony_welcome_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
% The figure can be deleted now
delete(handles.figure1);

% --- Executes on button press in pushbuttonDone.
function pushbuttonDone_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonDone (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%Save and export these values
handles.expinfo{1}=handles.exptime;
handles.expinfo{2}=handles.expname;
handles.expinfo{3}=handles.expexp;
handles.expinfo{4}=handles.expnotes;
handles.output = handles.expinfo;
guidata(hObject, handles);
uiresume(handles.figure1);

function editName_Callback(hObject, eventdata, handles)
% hObject    handle to editName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of editName as text
%        str2double(get(hObject,'String')) returns contents of editName as a double
handles.expname=strtrim(get(hObject, 'String'));
% Save the new value
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function editName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editExp_Callback(hObject, eventdata, handles)
% hObject    handle to editExp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.expexp=strtrim(get(hObject, 'String'));
% Save the new value
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function editExp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editExp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editNotes_Callback(hObject, eventdata, handles)
% hObject    handle to editNotes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of editNotes as text
%        str2double(get(hObject,'String')) returns contents of editNotes as a double
handles.expnotes=strtrim(get(hObject, 'String'));
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function editNotes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editNotes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editDate_Callback(hObject, eventdata, handles)
% hObject    handle to editDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of editDate as text
%        str2double(get(hObject,'String')) returns contents of editDate as a double
handles.expdate=strtrim(get(hObject, 'String'));
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function editDate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isequal(get(handles.figure1, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(handles.figure1);
else
    % The GUI is no longer waiting, just close it
    delete(handles.figure1);
end


% --- Monitors if user hits "enter"
%keypress_execute takes key button and uses to execute callback
%eventdata collects the keystroke
function keypress_execute(hObject,eventdata)
%brings in the handles structure in to the function
handles = guidata(hObject); %load handles
key= eventdata.Key;
if(strcmp(key,'return')) %user pressed 'return/enter'
    %set "Done" button as new source
    hObject = handles.pushbuttonDone; 
    pushbuttonDone_Callback(hObject, [], handles);
end
