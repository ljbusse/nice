function varargout = colony_tutorial(varargin)
% colony_tutorial is called from NICE_GUI_xxx
%       It is displayed from the About diaglog in the Help menu
%       Colony tutorial is a series of images and text describing the
%       basic operation of NICE.
%
%       Example:
%       colony_tutorial
%
%     NICE was developed by:
%     Matthew Clarke* (matthew.clarke@nist.gov) and 
%     Jeeseong Hwang (jeeseong.hwang@nist.gov)
%     Optical Technology Division
%     National Institute of Standards and Technology
%     Gaithersburg, MD 20899
% 
%     In collaboration with:
%     Robert Burton and Moon Nahm, University of Alabama � Birmingham
% 
%     Citation:
%     Matthew L. Clarke, Robert L. Burton, A. Nayo Hill, Maritoni Litorja, 
%     Moon H. Nahm,Jeeseong Hwang, 
%     "Low-Cost, High-Throughput, Automated Counting of Bacterial Colonies" 
%     Cytometry A, 77A, 790-797 (2010)
%
%     *Program code was developed by and correspondence should be addressed
%     to Matthew Clarke.  Correspondence may be sent to nice@nist.gov.
% 
%     Support for this project was provided in part by PATH.  The views 
%     expressed by the authors do not necessarily reflect the views of 
%     PATH.
%
%     Created and modified, 2008-2009
%
%  Legal Disclaimer:
%  NIST�s Integrated Colony Enumerator (NICE) software was developed at the
%  National Institute of Standards and Technology by employees of the
%  Federal Government in the course of their official duties. Pursuant to
%  Title 17 Section 105 of the United States Code this software is not
%  subject to copyright protection and is in the public domain. Support for
%  this project was provided in part by PATH.  The views expressed by the
%  developers of the software do not necessarily reflect the views of PATH.
%  NICE is an experimental system. Neither NIST nor PATH assumes any
%  responsibility whatsoever for its use by other parties, and makes no
%  guarantees, expressed or implied, about its quality, reliability, or any
%  other characteristic. We would appreciate acknowledgment if the software
%  is used. This software can be redistributed and/or modified freely
%  provided that any derivative works bear some notice that they are
%  derived from it, and any modified versions bear some notice that they
%  have been modified. 
%
% COLONY_TUTORIAL M-file for colony_tutorial.fig
%      COLONY_TUTORIAL, by itself, creates a new COLONY_TUTORIAL or raises the existing
%      singleton*.
%
%      H = COLONY_TUTORIAL returns the handle to a new COLONY_TUTORIAL or the handle to
%      the existing singleton*.
%
%      COLONY_TUTORIAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COLONY_TUTORIAL.M with the given input arguments.
%
%      COLONY_TUTORIAL('Property','Value',...) creates a new COLONY_TUTORIAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before colony_tutorial_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to colony_tutorial_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help colony_tutorial

% Last Modified by GUIDE v2.5 19-Jun-2009 12:48:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @colony_tutorial_OpeningFcn, ...
                   'gui_OutputFcn',  @colony_tutorial_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before colony_tutorial is made visible.
function colony_tutorial_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to colony_tutorial (see VARARGIN)
[handles]=Create_Strings(hObject,handles);
handles.current_slide=1;
set(handles.textDescription, 'String', handles.TutorialText{handles.current_slide});
set(handles.textLower, 'String', handles.LowerText{handles.current_slide});
set(handles.textSlideNumber, 'String', ['Slide ', sprintf('%2.0f', handles.current_slide), ' of ', sprintf('%2.0f', length(handles.TutorialText))]);
Itutorial=imread(handles.TutorialImages{handles.current_slide});
axes(handles.axesTutorial),imshow(Itutorial,[]);
axis off          % Remove axis ticks and numbers
axis image        % Set aspect ratio to obtain square pixels
% Choose default command line output for colony_tutorial
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes colony_tutorial wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = colony_tutorial_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
% varargout{1} = handles.output;
% The figure can be deleted now
delete(handles.figure1);

% --- Create_Strings creates a cell containing all descriptive text used in
%     the tutorial
function [handles]=Create_Strings(hObject,handles)
handles.TutorialText=cell(10,1);
handles.TutorialText{1}={'NICE is designed to count colonies from multiple regions of interest (ROIs).', ...
    'E.g., 24 "streaks" of stained colonies on an agar plate.',...
    'Images can be acquired by digital camera or desktop scanner.',...
    '',...
    'The primary application of NICE is for opsonization killing assays.'};
handles.TutorialText{2}={'When NICE launches it will display a login screen.',...
    'Users can input information about the experimental details.',...
    'If no information is given, values of "none" are entered by NICE.',...
    'The values entered here are exported along with the results.'};
handles.TutorialText{3}={'The toolbar is uesd to open images and explore them.',...
    '',...
    'Methods exist to change the parent ROI, move many ROIs, or change the contrast mode.',...
    '',...
    'Finally, buttons at the end switch the counted image to original, processed, and highlighted views, respectively.'};
handles.TutorialText{4}={'Below are examples of the different views and corresponding icon.',...
    'The original view is always available.',...
    'Processed images are only available after counting a single file.',...
    'Highlighted images are available after counting (including batch counting).'};
handles.TutorialText{5}={'After opening a file the ROIs are drawn.',...
    'If the user changes the number of rows of columns or moves the parent ROI,',...
    'the ROIs are recalculated.',...
    'To move an ROI, simply click anywhere within the ROI and drag it to its new position.',...
    'The last ROI clicked will display small rectangles at its corners.',...
    'These handles can be grabbed to resize the ROI.'};
handles.TutorialText{6}={'ROI shapes can be changed using the menu.',...
    'Parent or all individual ROIs can be rectangular or elliptical.',...
    '',...
    '',...
    'Choose the appropriate shape for your sample.',...
    ''};
handles.TutorialText{7}={'A histogram is displayed with every image.',...
    'It represents the population of pixels with a given gray level.',...
    '0 corresponds to black pixels, while 255 is white.',...
    '',...
    'NICE fits the histogram to determine the mean background level.',...
    'During counting, the threshold level is queried to determine if a count is valid.'};
handles.TutorialText{8}={'The "Count" button starts the process of counting all loaded images.',...
    'The time to count images is heavily dependent on the computer as well as the image size.',...
    'The "Step time" text at the top of NICE updates after each image is counted and is cumulative for batch counting.',...
    'The counts will be displayed in the left-hand side of each ROI.',...
    '"With colony sizes" will export the mean colony size along with the counts.'};
handles.TutorialText{9}={'The "Parameters" panel is used to modify the grid of ROIs and the counting algorithm.',...
    'Activating the parent ROI causes  analysis ROIs to be drawn within one region of the image.',...
    'The background threshold can be dynamically calculated by fitting the histogram or manually entered.',...
    'Resolution changes blurring and filtering of the image. Changing this paramater is mostly appropriate for scanned images.'};
handles.TutorialText{10}={'Navigation buttons are used to move between the displayed images or results.',...
    'The tab in the "Stored Data" panel determines whether navigation occurs between images or results.',...
    '',...
    'When in batch processing, the ROIs of the currently displayed image can be copied',...
    'to all other loaded images.'};
handles.TutorialText{11}={'"Stored Data" contains two tabs - Images and Results.',...
    'The "Images" tab lists all loaded files. Double-clicking on an Image name will open that file.',...
    'The "Results" tab lists all counted files using a number before the filename.',...
    'Double-clicking on an Result name will open that file and display the result.',...
    'Results can be exported to Excel or as a .txt file.',...
    'Results (but not Images) can be deleted (one or multiple) or cleared (all).'};
handles.TutorialText{12}={'Details about exporting.',...
    'If the filename exists, the user will be asked if they wish to overwrite it.',...
    'In the case of Excel files only the sheet within the file will be overwritten.',...
    'Excel files will then ask for a sheet name.',...
    'If the sheet exists (even if it is blank), the user will be asked if they wish to overwrite it.',...
    'Only this sheet will be overwritten.  Other sheets will remain.'};



handles.LowerText=cell(12,1);
handles.LowerText{1}={'Please see the manual for complete details.'};
handles.LowerText{2}={'The login information can be updated by clicking on the icon in the NICE main window.',...
    'Updated information is applied to all results after the update.'};

handles.LowerText{4}={'The processed image show the extended local minima.',...
    'The highlighted image shows the center of this minima.'};
handles.LowerText{5}={'ROIs are colored red while dragging and are colored green while resizing.'};
handles.LowerText{6}={'ROI shapes should not be change between images in batch runs.',...
    'The last shapes selected will be applied to all images.'};
handles.LowerText{7}={'Clicking "Log Scale" will rescale the y-axis.',...
    'Histogram fits and lines are not displayed during batch counting.'};
handles.LowerText{8}={'Colony sizing is a rough estimation for comparison only.',...
    'Colony areas are reported in pixels.'};
handles.LowerText{9}={'The number of rows, columns, and presence of a parent ROI must be the same for all images loaded in a batch'};

handles.LowerText{11}={'When loading a Result file, any other loaded Images are discarded.',...
    'NICE will behave as if a single file is loaded.  Recounting generates a new Result.'};
handles.LowerText{12}={'Each ROI will be given its own line in the exported file.',...
    'ROIs are numbered from the top-left and down the column.'};

handles.TutorialImages=cell(12,1);
handles.TutorialImages{1}='NICE-Tutorial-24plate.pcx';
handles.TutorialImages{2}='NICE-Tutorial-Opening.pcx';
handles.TutorialImages{3}='NICE-Tutorial-Toolbar.jpg';
handles.TutorialImages{4}='NICE-Tutorial-Views.pcx';
handles.TutorialImages{5}='NICE-Tutorial-Resize.png';
handles.TutorialImages{6}='NICE-Tutorial-ROI_shape.png';
handles.TutorialImages{7}='NICE-Tutorial-Histogram.pcx';
handles.TutorialImages{8}='NICE-Tutorial-Counting.pcx';
handles.TutorialImages{9}='NICE-Tutorial-Parameters.pcx';
handles.TutorialImages{10}='NICE-Tutorial-Navigation.pcx';
handles.TutorialImages{11}='NICE-Tutorial-StoredData.pcx';
handles.TutorialImages{12}='NICE-Tutorial-Export.png';

% --- Executes on button press in pushbuttonClose.
function pushbuttonClose_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonClose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figure1);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figure1);

% --- Executes on button press in pushbuttonForward.
function pushbuttonForward_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonForward (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles]=navigate_button(hObject, handles, 1); %go forward one slide
guidata(hObject,handles)

% --- Executes on button press in pushbuttonBack.
function pushbuttonBack_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles]=navigate_button(hObject, handles, -1); %go back one slide
guidata(hObject,handles)


%----navigate_button------
%moves through slides.  called by several pushbuttons
function [handles]=navigate_button(hObject, handles, direction)
if(min(direction~=[-1 1])) %is direction valid?
    errordlg('Error with navigation direction','Navigation Error', 'modal')
%if valid, check if already at the "new" image
elseif((direction==-1 && handles.current_slide==1) || (direction==1 && handles.current_slide==length(handles.TutorialText)));
else
    %Go to the appropriate slide
    handles.current_slide=handles.current_slide+direction;
    set(handles.textDescription, 'String', handles.TutorialText{handles.current_slide});
    set(handles.textLower, 'String', handles.LowerText{handles.current_slide});
    set(handles.textSlideNumber, 'String', ['Slide ', sprintf('%2.0f', handles.current_slide), ' of ', sprintf('%2.0f', length(handles.TutorialText))]);
    Itutorial=imread(handles.TutorialImages{handles.current_slide});
    axes(handles.axesTutorial),imshow(Itutorial,[]);
    axis off          % Remove axis ticks and numbers
    axis image        % Set aspect ratio to obtain square pixels
end
