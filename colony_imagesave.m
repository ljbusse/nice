function varargout = colony_imagesave(varargin)
% colony_imagesave is called from NICE_GUI_xxx
%       It displays a dialog to determine which image should be exported.
%
%       Example:
%       savechoice=colony_imagesave;
%
%     NICE was developed by:
%     Matthew Clarke* (matthew.clarke@nist.gov) and 
%     Jeeseong Hwang (jeeseong.hwang@nist.gov)
%     Optical Technology Division
%     National Institute of Standards and Technology
%     Gaithersburg, MD 20899
% 
%     In collaboration with:
%     Robert Burton and Moon Nahm, University of Alabama � Birmingham
% 
%     Citation:
%     Matthew L. Clarke, Robert L. Burton, A. Nayo Hill, Maritoni Litorja, 
%     Moon H. Nahm,Jeeseong Hwang, 
%     "Low-Cost, High-Throughput, Automated Counting of Bacterial Colonies" 
%     Cytometry A, 77A, 790-797 (2010)
%
%     *Program code was developed by and correspondence should be addressed
%     to Matthew Clarke.  Correspondence may be sent to nice@nist.gov.
% 
%     Support for this project was provided in part by PATH.  The views 
%     expressed by the authors do not necessarily reflect the views of 
%     PATH.
%
%     Created and modified, 2008-2009
%
%  Legal Disclaimer:
%  NIST�s Integrated Colony Enumerator (NICE) software was developed at the
%  National Institute of Standards and Technology by employees of the
%  Federal Government in the course of their official duties. Pursuant to
%  Title 17 Section 105 of the United States Code this software is not
%  subject to copyright protection and is in the public domain. Support for
%  this project was provided in part by PATH.  The views expressed by the
%  developers of the software do not necessarily reflect the views of PATH.
%  NICE is an experimental system. Neither NIST nor PATH assumes any
%  responsibility whatsoever for its use by other parties, and makes no
%  guarantees, expressed or implied, about its quality, reliability, or any
%  other characteristic. We would appreciate acknowledgment if the software
%  is used. This software can be redistributed and/or modified freely
%  provided that any derivative works bear some notice that they are
%  derived from it, and any modified versions bear some notice that they
%  have been modified. 
%
% COLONY_IMAGESAVE M-file for colony_imagesave.fig
%      COLONY_IMAGESAVE, by itself, creates a new COLONY_IMAGESAVE or raises the existing
%      singleton*.
%
%      H = COLONY_IMAGESAVE returns the handle to a new COLONY_IMAGESAVE or the handle to
%      the existing singleton*.
%
%      COLONY_IMAGESAVE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COLONY_IMAGESAVE.M with the given input arguments.
%
%      COLONY_IMAGESAVE('Property','Value',...) creates a new COLONY_IMAGESAVE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before colony_imagesave_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to colony_imagesave_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help colony_imagesave

% Last Modified by GUIDE v2.5 01-Oct-2008 16:57:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @colony_imagesave_OpeningFcn, ...
                   'gui_OutputFcn',  @colony_imagesave_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before colony_imagesave is made visible.
function colony_imagesave_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to colony_imagesave (see VARARGIN)
handles.savechoice=1; %set default choice as annotated image
%set "enter/return" to operate just like clikcing "OK"
set([handles.figure1, handles.radiobuttonAnnotate, handles.radiobuttonPlain, handles.radiobuttonNICE],'KeyPressFcn',@keypress_execute); 
handles.output = handles.savechoice;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes colony_imagesave wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = colony_imagesave_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
% The figure can be deleted now
delete(handles.figure1);

% --- Executes on button press in pushbuttonOK.
function pushbuttonOK_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figure1);

% --- Executes when selected object is changed in uipanelChoice.
function uipanelChoice_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanelChoice 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
switch get(hObject,'Tag')   % Get Tag of selected object
    case 'radiobuttonAnnotate'  % Code for when Annotate button is selected.
        handles.savechoice=1;
    case 'radiobuttonPlain'
        handles.savechoice=2;
    case 'radiobuttonNICE'
        handles.savechoice=3;
    otherwise
        errordlg('Radio button malfunction','Image save error','modal')
end
handles.output = handles.savechoice;
guidata(hObject,handles)


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.savechoice=0; %if cancel out, then do not save
handles.output = handles.savechoice;
guidata(hObject,handles)
uiresume(handles.figure1);

% --- Monitors if user hits "enter"
%keypress_execute takes key button and uses to execute callback
%eventdata collects the keystroke
function keypress_execute(hObject,eventdata)
%brings in the handles structure in to the function
handles = guidata(hObject); %load handles
key= eventdata.Key;
if(strcmp(key,'return')) %user pressed 'return/enter'
    %set "OK" button as new source
    hObject = handles.pushbuttonOK; 
    pushbuttonOK_Callback(hObject, [], handles);
end


